﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using TMPro;
using UnityEngine;

public class ControlPhrase : MonoBehaviour
{
    private TextMeshPro _mesh;
    private MeshRenderer _renderer;
    private slConfig _cnfg; //記述の簡潔化と呼び出し回数の多さを考慮して

    private Transform _parent; //親グループのTransform

    //動きのモード   
    //  0: 球/ビルボード/速度ランダム
    //  1: 螺旋/中心向き/均一の速度
    // -1: 生成時。中央に密集している
    private int _mode = -1;
    private int _nextMode = 0;
    private Vector3[] _positions;
    private Vector3[] _lookAts;
    private float[] _speeds;

    private float _changeDuration = 1.5f;   //モード変更にかける時間
    private float _changeDurationF;   //モード変更にかけるフレーム
    private float _changePast = 0;  //今のモード変更にかかっている時間
    private bool _changing = false; //モード変更中かどうか
    private Vector3 _changeStartPos;    //変更開始時の位置

    public float theta { get; private set; }    //このメッシュで螺旋がすすむ角度

    private float _basesize = 20;


    private void Awake()
    {
        _cnfg = GenerateGroups.cnfg;

        _mesh = gameObject.GetComponent<TextMeshPro>();
        _renderer = gameObject.GetComponent<MeshRenderer>();
        _parent = transform.parent;

        //modeの数だけ各値を用意
        _positions = new Vector3[GenerateGroups.modeCount];
        _lookAts = new Vector3[GenerateGroups.modeCount];
        _speeds = new float[GenerateGroups.modeCount];

        _positions[0] = transform.localPosition + (Random.onUnitSphere * 10);
        _lookAts[0] = GenerateGroups.camDirection;
        _lookAts[1] = new Vector3(_parent.position.x, transform.position.y, _parent.position.z);
    }



    /// 各種情報を受け取って初期設定
    public void Init(string Phrase, float SpyralOffset, Color clr)
    {
        _mesh.text = Phrase;
        _mesh.ForceMeshUpdate();
        _mesh.color = new Color(randomClr(clr.r), randomClr(clr.g), randomClr(clr.b));

        _changeDurationF = _changeDuration * _cnfg.cf_fps;


        //スピード設定
        _speeds[0] = Random.Range(_cnfg.cf_spdMin, _cnfg.cf_spdMax);
        _speeds[0] *= (Random.Range(0, 2) - 0.5f) * 2;  //正負をランダムに
        _speeds[1] = GenerateGroups.cnfg.cf_spd;



        //螺旋モード時の位置設定
        float width = _mesh.preferredWidth * _mesh.fontSize / _mesh.fontSizeMax+1.0f;   //フォントサイズによって大きさ補正
        
        theta = 2f*Mathf.Asin(width / 2f / _cnfg.cf_spyralRad); //メッシュ横幅を円弧に張る弦とした場合の角度を求める
        float r = _cnfg.cf_spyralRad * Mathf.Cos(theta / 2);                    //中心から弦までの距離
        _positions[1] = new Vector3(
             Mathf.Cos(SpyralOffset + theta / 2) * r,                       //x,z: 角度オフセット＋自身のメッシュ左半分の角度（アンカーは中心なので）から求める
             _cnfg.cf_spyralTopY - SpyralOffset * _cnfg.cf_spyralDeltaY,    //y: 最初の単語の高さー進んだ角度＊下がり幅
             Mathf.Sin(SpyralOffset + theta / 2) * r);
        // 螺旋モード時に地面に垂直になるよう注目点の高さ調整
        _lookAts[1].y = _positions[1].y + _parent.transform.position.y;
    }



    //// 色にブレをもたせる
    private float randomClr(float rgb)
    {
        float rtnRGB = rgb + (Random.value-0.5f)/2.5f;
        if (rtnRGB > 1) rtnRGB = 1f;
        if (rtnRGB < 0) rtnRGB = 0f;
        return rtnRGB;
    }



    void Update()
    {
        /// モードごとに動きの制御
        int MODE = GenerateGroups.currentMode;
        if (!_changing && MODE == _mode)  //モード通りの動作
        {
            transform.RotateAround(_parent.position, _parent.up, _speeds[MODE]);
            transform.LookAt(_lookAts[MODE]);
        }
        else if(!_changing && MODE != _mode)  //モード変更開始
        {
            _changing = true;
            _nextMode = MODE;
            _changeStartPos = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
        }
        else //モード変更中
        {
            //Debug.Log($"{_changePast},{_changeStartPos},{_positions[_nextMode] - _changeStartPos},{_changeDurationF}");
            Vector3 pos = MyEasingVector.easeOutCirc(_changePast, _changeStartPos, _positions[_nextMode] - _changeStartPos, _changeDurationF);   //経過フレーム、開始位置、移動ベクトル、かける時間
            transform.localPosition = pos;
            transform.LookAt(_lookAts[_nextMode]);

            if (_changePast >= _changeDurationF)
            {
                _changing = false;
                _mode = _nextMode;
                _changePast = 0;
            }
            else
            {
                _changePast++;
            }
        }

        if(MODE == 0)
        {
            if (GenerateGroups.camMoving || _changing )
            {
                _lookAts[0] = GenerateGroups.camDirection;
            }
        }
    }



}
