﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateOneGroup : MonoBehaviour
{
    // 1キャラ分のBiRDSを生成する
    //

    [SerializeField] private GameObject[] PrismPrefabs = default;
    [SerializeField] private GameObject PhrasePrefab = default;

    private Transform _centerTf;
    private GameObject[] _pharaseObjAry;
    public string[] _phraseAry { get; set; }
    private Color _clr;

    public static float baseSize = -1;       //基準のフレーズメッシュサイズ
    public static float coefficient;    //フレーズメッシュサイズを基準と同じにするための係数


    //// 初期化して情報受け取って生成
    public void init(int groupID, string[] phraseAry, Color clr)
    {
        _phraseAry = phraseAry;
        _clr = clr;

        GameObject center = Instantiate(PrismPrefabs[groupID], transform);
        _centerTf = center.transform;
        _centerTf.localScale = new Vector3(12, 12, 12);
        _pharaseObjAry = new GameObject[_phraseAry.Length];

        generatePhrase();
    }


    private void Update()
    {
        _centerTf.Rotate(0, 0, 0.5f);
    }


    //// フレーズを生成
    public void generatePhrase()
    {
        coefficient = -1;   //リセット
        float spyralOffset = -Mathf.PI/2f;  //最初のフレーズが正面になるよう調整
        for (int i = 0; i < _phraseAry.Length; i++)
        {
            _pharaseObjAry[i] = Instantiate(PhrasePrefab, transform.position, transform.rotation, transform);  //まずオブジェクトを作成
            ControlPhrase cp = _pharaseObjAry[i].GetComponent<ControlPhrase>(); //複製しコンポーネント取得
            cp.Init(_phraseAry[i], spyralOffset, _clr);
            spyralOffset += cp.theta; //この単語で進んだ螺旋の角度を足していく
        }
    }



    /// フレーズを更新
    public void updatePhrase(string phrase)
    {
        string[] tmpAry = (string[])_phraseAry.Clone();
        for (int i = 0; i < tmpAry.Length; i++)
        {
            Destroy(_pharaseObjAry[i]);     //現在のフレーズオブジェクト1つ削除
            _pharaseObjAry[i] = default;
            if (i == 0) continue;
            _phraseAry[i] = tmpAry[i - 1];  //フレーズデータをいっこずつ後ろにずらす。最後尾は引き継がない（削除）
        }
        _phraseAry[0] = phrase;             //先頭に新しいフレーズ
        generatePhrase();
    }
}
