﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;


public class RenderMovie : MonoBehaviour
{
    // 
    // newに画像生成  
    // beforeLastにlastのmvをコピー、lastにnewのmvをコピー、newをのmvとimgを削除

    // 画像生成中にエラー → その時点で中止してnew内をリセット
    // 動画生成中にエラー → その時点で中止してnew内をリセット


    [SerializeField] private GameObject RenderCamera = default;

    //// パラメータ
    public int width { set; get; } = 1920;
    public int height { set; get; } = 1080;
    public int eachMvFrame { set; get; } = 30;
    public int camMoveFrame { set; get; } = 30;
    public int finished { set; get; } = 0;
    public int fps { set; get; } = 30;

    private string _finishFlag;
    private string _errorContent;

    private string _renderDir_Rt = @"Render\";
    private string _renderDir_Rd = @"Render\";
    private string _ffmpegPath = @"C:\Software\ffmpeg\ffmpeg.exe";
    private string[] _newness = { "new\\", "last\\", "beforeLast\\" };
    private string _imgDir = @"img\";
    private string _mvDir = @"mv\";

    //// 画像関連
    private RenderTexture _renTex;
    private Texture2D _tex2D;
    private int _renderImgID = 0;
    private string _imgName;        //画像のフレーム数を除いた名前
    private string _lastImgName;    //リネーム用

    //// 動画関連
    private int _clipFrame;         //動画の頭ずらしのフレーム数
    private int _totalFrame;
    private int _encodedMvNum = 0;
    



    //// 初期設定
    public void Init()
    {
        _finishFlag = "non";

        _renderDir_Rt = GenerateGroups.dataDirPath_Rt + _renderDir_Rt;
        _renderDir_Rd = GenerateGroups.dataDirPath_Rd + _renderDir_Rd;

         _renTex = RenderCamera.GetComponent<Camera>().targetTexture;
        _tex2D = new Texture2D(width, height, TextureFormat.ARGB32, false, false);  //指定サイズのtexture2D生成

        _clipFrame = eachMvFrame + camMoveFrame;
        _totalFrame = (eachMvFrame + camMoveFrame) * 11;
    }




    //// 
    public void startRender()
    {
        //// 現在のフラグによって処理を分岐
        // 初回：画像生成ループスタート
        if (_finishFlag == "non")
        {
            StartCoroutine(SaveImgFromTex("test"));
        }
        // 画像生成完了後：動画生成ループスタート
        else if (_finishFlag == "img")
        {
            GenerateGroups.output.Log("映像レンダリング開始");
            StartEncodeMv($"{DateTime.Today:yyMMdd_HHmm}_mv");
        }
        // 動画生成完了後：バックアップスタート
        else if (_finishFlag == "movie")
        {
            GenerateGroups.output.Log("映像バックアップ開始");
            BackUp();
            BackUpRt();
            GenerateGroups.output.Log("映像バックアップ終了");
        }
        // バックアップ完了後：戻り値を変えて終了
        else if (_finishFlag == "backup")
        {
            finished = 1;
            _finishFlag = "all";
        }
        else if(_finishFlag == "error")
        {
            GenerateGroups.output.Log($"{_errorContent}中にエラーが生じたので、更新を中止します.");
            finished = -1;
        }
    }


    //// 指定のパスにrendertextureを画像で書き出し
    IEnumerator SaveImgFromTex(string imgName)
    {
        if(_renderImgID == 0)
        {
            GenerateGroups.output.Log("画像レンダリング開始");
            slIO_plus.ResetDirectory(_renderDir_Rd + _imgDir);
        }


        // 名前決め
        _imgName = imgName + "00_";
        string name = _imgName + _renderImgID.ToString("D6") + ".png";


        // RenderTextureをTexture2Dに読みこみ
        RenderTexture.active = _renTex;  //アクティブに指定
        _tex2D.ReadPixels(new Rect(0, 0, _renTex.width, _renTex.height), 0, 0, true); //Texture2Dに読みこむ
        RenderTexture.active = null;     //解放                                                                   


        // pngに書き出し
        try
        {
            File.WriteAllBytes(_renderDir_Rd+_imgDir+name, _tex2D.EncodeToPNG());  //PNG形式のByte配列に変換したものをファイルとして保存
        }
        catch(Exception e)
        { 
            GenerateGroups.output.Log("[ERROR] Couldn't save\"" + name + "\". \n"+ e.Message);
            _errorContent = "画像の生成";
            _finishFlag = "error";

            yield break;
        }


        // 終了判断をして画像IDを進める
        if (_renderImgID + 1 < _totalFrame)
            _renderImgID++;
        else
        {
            _finishFlag = "img";
            GenerateGroups.output.Log("画像レンダリング終了");
        }


        // 一旦止まって次のフレームで再開
        //      yield return : 列挙子 ---（イテレーター）returnと同じように呼び出し元に返すけど処理は中断したまま。条件を満たすと途中から再開
        yield return new WaitForEndOfFrame();
    }




    private void StartEncodeMv(string clipName)
    {
        _finishFlag = "img_";

        slIO_plus.ResetDirectory(_renderDir_Rd + _mvDir + _newness[0]);

        System.Diagnostics.Process p = new System.Diagnostics.Process();
        p.StartInfo.FileName = _ffmpegPath;
        p.EnableRaisingEvents = true;
        p.Exited += (object o, EventArgs e) =>
        {
            _encodedMvNum++;

            //　動画数が合わなかったらエラーで中止
            if (Directory.GetFiles(_renderDir_Rd + _mvDir + _newness[0]).Length != _encodedMvNum)
            {
                _finishFlag = "error";
                _errorContent = "動画のエンコード";
                return;
            }

            _lastImgName = _imgName;
            _imgName = _imgName.Remove(_imgName.Length-3) + _encodedMvNum.ToString("D2") + "_";
                //if (_encodedMvNum < 1)
            if (_encodedMvNum < 11)
                renameImgs(p, clipName);
            else
            {
                _finishFlag = "movie";
                GenerateGroups.output.Log("映像レンダリング終了");
            }
        };
        EncodeMv(p, clipName+"00");
    }


    //// 動画を一回エンコード
    private void EncodeMv(System.Diagnostics.Process P, string Name)
    {
        string imgDir = _renderDir_Rd + _imgDir;
        string mvPath = _renderDir_Rd + _mvDir + _newness[0] + Name + ".mp4";
        if(fps == 30)
            P.StartInfo.Arguments = $"-y -r 30 -i {imgDir + _imgName}%06d.png -vframes {_totalFrame * 2} -vcodec libx264 -pix_fmt yuv420p {mvPath}";
        else if(fps == 60)
            P.StartInfo.Arguments = $"-y -r 60 -i {imgDir + _imgName}%06d.png -vframes {_totalFrame * 2} -vcodec libx264 -pix_fmt yuv420p {mvPath}";
        ////-y(上書き), -r(フレームレート), -i(入力ファイルパス), -vframes(総フレーム数), -vcodec(コーデック指定), -pix_fmt(エンコーダーのピクセルフォーマット), 
        GenerateGroups.output.Log("動画エンコード "+ Name+".mp4");

        P.Start();
    }



    //// 画像のIDをすべてずらす
    private void renameImgs(System.Diagnostics.Process P, string ClipName)
    {
        string imgDir = _renderDir_Rd +_imgDir;

        try
        {
            for (int i = 0; i < _totalFrame; i++)
            {
                int j = (i-_clipFrame >= 0) ? i-_clipFrame : i-_clipFrame+_totalFrame;              //IDをずらす
                File.Move($"{imgDir}{_lastImgName}{i:D6}.png", $"{imgDir}{_imgName}{j:D6}.png");    //画像名が同じだと書き換える前に上書きされたりでうまくいかないので別の名前にする
            }
            EncodeMv(P, ClipName + _encodedMvNum.ToString("D2"));
        }
        catch
        {
            GenerateGroups.output.Log("ERROR --- "+ ClipName + _encodedMvNum.ToString("D2"));
            _finishFlag = "error";
            _errorContent = "画像のリネーム";
        }
    }



    //// 映像のバックアップを更新
    private void BackUp()
    {
        string newPath = _renderDir_Rd + _mvDir + _newness[0];
        string lastPath = _renderDir_Rd + _mvDir + _newness[1];
        string beforeLastPath = _renderDir_Rd + _mvDir + _newness[2];

        // last→beforelast
        slIO_plus.ResetDirectory(beforeLastPath);
        slIO_plus.MoveAllFiles(lastPath, beforeLastPath);

        // new→last
        slIO_plus.ResetDirectory(lastPath);
        slIO_plus.MoveAllFiles(newPath, lastPath);

        // newリセット
        slIO_plus.ResetDirectory(newPath);

        _finishFlag = "backup";
    }


    //// リアルタイム用PCにもコピー
    private void BackUpRt()
    {
        string lastPath_Rd = _renderDir_Rd + _mvDir + _newness[1];
        string lastPath_Rt = _renderDir_Rt + _mvDir + _newness[1];
        slIO_plus.ResetDirectory(lastPath_Rt);
        slIO_plus.CopyAllFiles(lastPath_Rd, lastPath_Rt);
        

        string beforeLastPath_Rd = _renderDir_Rd + _mvDir + _newness[2];
        string beforeLastPath_Rt = _renderDir_Rt + _mvDir + _newness[2];
        slIO_plus.ResetDirectory(beforeLastPath_Rt);
        slIO_plus.CopyAllFiles(beforeLastPath_Rd, beforeLastPath_Rt);
    }

}
