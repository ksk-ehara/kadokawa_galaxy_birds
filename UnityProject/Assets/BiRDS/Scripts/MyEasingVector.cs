﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEasingVector : MonoBehaviour {

    //木村さんの独自クラス
    //t : 時間(進行度)
    //b : 開始の値(開始時の座標やスケールなど)
    //c : 開始と終了の値の差分
    //d : Tween(トゥイーン)の合計時間 

    // no easing, no acceleration
    public static Vector3 linearTween(float t, Vector3 b, Vector3 c, float d)
    {
        return c * t / d + b;
    }

    // Quadratic
    public static Vector3 easeInQuad(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return c * t * t + b;
    }

    public static Vector3 easeOutQuad(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return -c * t * (t - 2) + b;
    }

    public static Vector3 easeInOutQuad(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    }

    // Cubic
    public static Vector3 easeInCubic(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return c * t * t * t + b;
    }

    public static Vector3 easeOutCubic(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        t--;
        return c * (t * t * t + 1) + b;
    }

    public static Vector3 easeInOutCubic(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t * t + b;
        t -= 2;
        return c / 2 * (t * t * t + 2) + b;
    }

    // Quartic
    public static Vector3 easeInQuart(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return c * t * t * t * t + b;
    }

    public static Vector3 easeOutQuart(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return c * t * t * t * t + b;
    }

    public static Vector3 easeInOutQuart(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t * t * t + b;
        t -= 2;
        return -c / 2 * (t * t * t * t - 2) + b;
    }

    // Quintic
    public static Vector3 easeInQuint(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return c * t * t * t * t * t + b;
    }

    public static Vector3 easeOutQuint(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        t--;
        return c * (t * t * t * t * t + 1) + b;
    }

    public static Vector3 easeInOutQuint(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t * t * t * t + b;
        t -= 2;
        return c / 2 * (t * t * t * t * t + 2) + b;
    }

    // Sinusoidal
    public static Vector3 easeInSin(float t, Vector3 b, Vector3 c, float d)
    {
        return -c * (float)Mathf.Cos(t / d * (Mathf.PI / 2)) + c + b;
    }

    public static Vector3 easeOutSin(float t, Vector3 b, Vector3 c, float d)
    {
        return c * (float)Mathf.Sin(t / d * (Mathf.PI / 2)) + b;
    }

    public static Vector3 easeInOutSin(float t, Vector3 b, Vector3 c, float d)
    {
        return -c / 2 * (float)(Mathf.Cos(Mathf.PI * t / d) - 1) + b;
    }

    // Exponential
    public static Vector3 easeInExpo(float t, Vector3 b, Vector3 c, float d)
    {
        return c * (float)Mathf.Pow(2, 10 * (t / d - 1)) + b;
    }

    public static Vector3 easeOutExpo(float t, Vector3 b, Vector3 c, float d)
    {
        return c * (float)(-Mathf.Pow(2, -10 * t / d) + 1) + b;
    }

    public static Vector3 easeInOutExpo(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return c / 2 * (float)Mathf.Pow(2, 10 * (t - 1)) + b;
        t--;
        return c / 2 * (-(float)Mathf.Pow(2, -10 * t) + 2) + b;
    }

    // Circular
    public static Vector3 easeInCirc(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        return -c * ((float)Mathf.Sqrt(1 - t * t) - 1) + b;
    }

    public static Vector3 easeOutCirc(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d;
        t--;
        return c * (float)Mathf.Sqrt(1 - t * t) + b;
    }

    public static Vector3 easeInOutCirc(float t, Vector3 b, Vector3 c, float d)
    {
        t /= d / 2;
        if (t < 1) return -c / 2 * (float)(Mathf.Sqrt(1 - t * t) - 1) + b;
        t -= 2;
        return c / 2 * (float)(Mathf.Sqrt(1 - t * t) + 1) + b;
    }
}
