﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

//****************************************
// 設定ファイル読み込みクラス
//****************************************
//  使用クラス: _slReadWriteFile
//
//  パスを指定することでテキストファイルを読み込み、その中のイコールでつながれていた行を設定値と認識し
//  右辺を項目、左辺を値として読みこみインスタンス内に保持する
//  現状対応してるのはint, float, string, bool
//  各プロジェクトごとにConfig項目をかきかえる
//      ※項目の動的生成も考えたけど型複数使いたいし項目名が検索候補に出たほうが使いやすそうだから没


public class slConfig
{
    private slReadWriteFile _slReadWriteFile;

    //********************
    // Config項目
    //********************
    //      項目はプロジェクトによって増やす ※フィールド名は「cf_+項目名」
    //      プロパティにすることでinspectorに表示されないようにしている
    //      追加したいプロパティはここに追加するだけで後は勝手に処理する

    public int cf_fps { get; set; } = 1;
    public float cf_camMoveDuration { get; set; } = 1;
    public float cf_modeSpan { get; set; } = 1;


    public float cf_groupsRadius { get; set; } = 1;
    public float cf_groupsHeight { get; set; } = 1;
    public float cf_groupsSpiral { get; set; } = 1;
    public float[] cf_colorA { get; set; } = {1,1};
    public float[] cf_colorB { get; set; } = {1,1};
    public float[] cf_colorC { get; set; } = {1,1};
    public float[] cf_colorD { get; set; } = {1,1};
    public float[] cf_colorE { get; set; } = {1,1};
    public float[] cf_colorF { get; set; } = { 1, 1 };
    public float[] cf_colorG { get; set; } = { 1, 1 };
    public float[] cf_colorH { get; set; } = { 1, 1 };
    public float[] cf_colorI { get; set; } = { 1, 1 };
    public float[] cf_colorJ { get; set; } = { 1, 1 };
    public float cf_charaSize { get; set; } = 1;

    public float cf_spyralTopY { get; set; } = 1;       //螺旋の半径
    public float cf_spyralRad { get; set; } = 1;       //螺旋の半径
    public float cf_spyralDeltaY { get; set; } = 1;     //螺旋の縦の変化量

    public float cf_phraseSize { get; set; } = 1;       //フレーズの最小速度
    public float cf_spd { get; set; } = 1;       //フレーズの最大速度
    public float cf_spdMax { get; set; } = 1;       //フレーズの最大速度
    public float cf_spdMin { get; set; } = 1;       //フレーズの最小速度

    public int cf_mvWidth { get; set; } = 1;
    public int cf_mvHeight { get; set; } = 1;
    public float cf_EachMvLength { get; set; } = 1;

    public string cf_movieTag { get; set; } = "";
    public bool cf_rebootBS { get; set; } = true;

    public string cf_UserPostingPageURL { get; set; } = "";
    public string cf_UnityAccessURL{ get; set; } = "";

    public bool cf_demoMode { get; set; } = false;
    public int cf_iTryServerConnectMax { get; set; } = 1;


    //// コンストラクタ
    public slConfig(string path, bool ifNoSuchFileCreateNew = false)
    {
        _slReadWriteFile = new slReadWriteFile();

        if (File.Exists(path)) Get(path);
        else
        {
            if(ifNoSuchFileCreateNew)
                Set(path);              //指定ファイルが無くてオプション指定があったら生成
            else
                Console.WriteLine($"[slConfig] [{path}]というファイルは存在しません");
        }
    }


    //********************
    // 指定パスのファイルからConfig情報取得
    //********************
    public void Get(string path)
    {
        List<string> list = _slReadWriteFile.Read(path); //指定ファイルを1行ごとに読みこんだリスト

        /// Config項目に値を反映
        foreach (string line in list)
        {
            // 設定値の行以外を除外
            string l = line.Replace(" ", "").Replace("　", "");  // スペース削除
            if (l.ToLower() == "break") break;                  // break以下の行コメントなので無視
            if (!l.Contains("=") || l.IndexOf("//") == 0 || l.IndexOf("#") == 0) continue;  //"=”がない行と"//","#"で始まる行は無視

            // 項目名と値を取得（値がまだstringのまま）
            string item = "cf_" + l.Split('=')[0];
            string value = l.Split('=')[1];

            // itemと名前が一致するプロパティの情報取得
            PropertyInfo prInfo = typeof(slConfig).GetProperty(item);
            if (prInfo == null)
            {
                Console.WriteLine($"[slConfig] [{item}]という項目が設定されていません");
                continue;
            }

            // 型をstringから変換してConfig項目に代入
            try
            {
                if (prInfo.PropertyType == typeof(int))         // itemと名前が一致するメンバの型がintの時
                    prInfo.SetValue(this, int.Parse(value));    //　そのメンバの値にvalueの型を合わせて代入
                else if (prInfo.PropertyType == typeof(float))
                    prInfo.SetValue(this, float.Parse(value));
                else if (prInfo.PropertyType == typeof(bool))
                    prInfo.SetValue(this, bool.Parse(value));
                else if (prInfo.PropertyType == typeof(string[]))
                    prInfo.SetValue(this, value.Split(','));
                else if (prInfo.PropertyType == typeof(int[]))
                {
                    string[] sAry = value.Split(',');
                    int[] iAry = new int[sAry.Length];
                    for (int i = 0; i < sAry.Length; i++)
                    {
                        iAry[i] = int.Parse(sAry[i]);
                    }
                    prInfo.SetValue(this, iAry);
                }
                else if (prInfo.PropertyType == typeof(float[]))
                {
                    string[] sAry = value.Split(',');
                    float[] fAry = new float[sAry.Length];
                    for (int i = 0; i < sAry.Length; i++)
                    {
                        fAry[i] = float.Parse(sAry[i]);
                    }
                    prInfo.SetValue(this, fAry);
                }
                else //string
                    prInfo.SetValue(this, value);
            }
            catch (Exception e)
            {
#if UNITY_EDITOR
                UnityEngine.Debug.Log($"[slConfig] 例外 - {e}\n 項目[ {item} ]に対して、値[ {value} ]は無効です");
#else
                Console.WriteLine($"[slConfig] 例外 - {e}\n 項目[ {item} ]に対して、値[ {value} ]は無効です");
#endif            
            }
        }
    }




    //********************
    // 現状のConfigでファイルに書き込み
    //********************
    //      将来的にはコメントアウトの行をとばしてとかにしたいがとりあえずは
    //      現状のConfigクラスからリストを作り書きこみ先ファイルのbreak以下をくっつける
    public void Set(string path)
    {
        //// 現在のConfigから書き込み用のリストを作る
        var setList = new List<string>();   //書き込み用リスト
        var propertyInfoAry = typeof(slConfig).GetProperties();   //プロパティ情報の配列取得
        foreach(PropertyInfo prInfo in propertyInfoAry)
        {
            if (prInfo.Name.Substring(0, 3) != "cf_") continue; //cf_がついているものがConfig項目なのでそれ以外はスキップ
            string item = prInfo.Name.Substring(3);
            string value = "";
            if(prInfo.PropertyType == typeof(string[]) || prInfo.PropertyType == typeof(int[]) || prInfo.PropertyType == typeof(float[]))
            {
                UnityEngine.Debug.Log("配列の書き込みはまだ実装されていません");
                //UnityEngine.Debug.Log(prInfo.GetValue(this)); //これが思った通りの値が来ない？
            }
            else
            {
                value = prInfo.GetValue(this).ToString();
            }
            setList.Add(item + " = " + value);
        }
        setList.Add("");


        //// break以下の行を取得してsetListに加える
        var list = _slReadWriteFile.Read(path);
        bool broken = false;    //breakの行を過ぎたかどうか
        foreach (string line in list)
        {
            if (!broken)
            {
                string l = line.Replace(" ", "").Replace("　", "");  // スペース削除
                if (l.ToLower() == "break")
                {
                    broken = true;
                    setList.Add("BREAK");
                }
                continue;
            }
            setList.Add(line);
        }

        //// 実際に書き込み
        _slReadWriteFile.Write(path, setList);
    }

}
