﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Data;
using System.Text;

public class sluOutput_Log : MonoBehaviour
{
    // Debug.Logのかわりに使うことでDebug.Logに書きだしつつ同じ内容を外部のテキストファイルにも出力するクラス
    // インスタンス生成し、あとは使いたいところでLogメソッド
    // 日付けごとのフォルダ生成やファイル分けはChangeDirFile_yyyyMM_MMddのようなメソッドを都度用意する



    private string _logPath = @"C:\KADOKAWA_data\Log\";
    private string _today = "20200930";
    private string _todayPath = @"C:\KADOKAWA_data\Log\202009\20200930_log.txt";


    // コンソールとlogファイル療法に出力する
    public void Log(string contents)
    {
        // ディレクトリは月ごと、ファイルは日ごとに切り替え
        ChangeDirFile_yyyyMM_MMdd();


        Debug.Log(contents);

        using (var fs = new FileStream(_todayPath, FileMode.Append, FileAccess.Write))
        {
            using (var sw = new StreamWriter(fs))
            {
                sw.WriteLine(contents);
            }
        }
    }




    //// ディレクトリは月ごと、ファイルは日ごとに切り替え
    private void ChangeDirFile_yyyyMM_MMdd()
    {
        DateTime d = DateTime.Today;

        // 月が替わった時フォルダを変える
        if (d.ToString("MM") != _today.Substring(4, 2))
        {
            if (!Directory.Exists(_logPath + d.ToString("yyyyMM")))
                Directory.CreateDirectory(_logPath + d.ToString("yyyyMM"));
        }

        // 日が替わった時ファイルパスを変える（ファイル生成はfilestreamでインスタンス生成時に勝手に行われる）
        if (d.ToString("MMdd") != _today.Substring(4, 4))  //日が替わった時
        {
            _todayPath = $"{_logPath}\\{d:yyyyMM}\\{d:yyyyMMdd}_log.txt";
            _today = d.ToString("yyyyMMdd");
        }
    }
}
