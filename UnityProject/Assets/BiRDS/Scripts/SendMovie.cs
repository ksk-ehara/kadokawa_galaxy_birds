﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SendMovie : MonoBehaviour
{
    //- - - - - - - - - - 
    //  完成した映像を各BrightSignに配付する

    public string mvDir { set; get; } = @"Render\mv\last\";

    private string _IPListPath = @"IPList.txt";
    private string _upLoaderPath = @"C:\Software\BrightSignCopy\BSFileUpLoader.exe";

    private List<string[]> _IPList;
    private string[] _mvList;
    System.Diagnostics.Process _prcs;
    private int _nextCopyIP = 0;


    public void Init()
    {
        _mvList = new string[] { "" };


        //// テキストファイルから情報取得
        _IPList = new List<string[]>();
        using (StreamReader sr = new StreamReader(GenerateGroups.dataDirPath_Rt + _IPListPath)) //リストの参照はBiRDS１優先
        {
            string line = null;
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Substring(0, 1) == "#" || line.Substring(0, 2) == "//") continue;  //コメントアウトは飛ばす
                string[] parts = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);  // =で分ける
                if (parts.Length == 2 && parts[0].Length == 14)
                {
                    _IPList.Add(new string[] { parts[0], parts[1] }); //IP:映像のID のペアで連想配列に記録
                    GenerateGroups.output.Log($"IP[{parts[0]}]に動画ID[{parts[1]}]");
                }
            }
        }


        //// コピーソフトの起動設定
        _prcs = new System.Diagnostics.Process();
        _prcs.StartInfo.FileName = _upLoaderPath;

        _prcs.EnableRaisingEvents = true;
        _prcs.Exited += (object o, EventArgs e) =>
        {
            GenerateGroups.output.Log($"コピー完了 [{_IPList[_nextCopyIP][0]} --- {_mvList[int.Parse(_IPList[_nextCopyIP][1])]}]");
            _nextCopyIP++;
            if (_nextCopyIP < _IPList.Count) //まだ配ってないIPがあったら次の配布処理へ
                SendOneMv(_nextCopyIP);
            else
            {
                GenerateGroups.output.Log("全てのコピーが完了");
                GenerateGroups.CloseApp();
            }
        };
        _prcs.ErrorDataReceived += (object o, System.Diagnostics.DataReceivedEventArgs e) =>
        {
            GenerateGroups.output.Log($"Copy Error");
        };
    }



    public void StartSendMv()
    {
        //// 配る映像のパスを含んだリスト取得
        _mvList = Directory.GetFiles(GenerateGroups.dataDirPath_Rd + mvDir, "*", SearchOption.AllDirectories);

        SendOneMv(_nextCopyIP);
    }



    public void SendOneMv(int num)
    {
        string ip = _IPList[num][0];
        int mvID = int.Parse(_IPList[num][1]);
        string rbt = GenerateGroups.cnfg.cf_rebootBS ? "-reboot" : "";

        if (GenerateGroups.demoMode)
            _prcs.StartInfo.Arguments = $"-debug -ip {ip} -rt  test  -uf {_mvList[mvID]} -log {GenerateGroups.dataDirPath_Rd}Log\\sendLog.txt";
        else
            _prcs.StartInfo.Arguments = $"-ip {ip} -rt  {GenerateGroups.cnfg.cf_movieTag} -uf  {_mvList[mvID]} -log {GenerateGroups.dataDirPath_Rd}Log\\sendLog.txt {rbt}";

        GenerateGroups.output.Log($"コピー開始 --- {_prcs.StartInfo.Arguments}");

        _prcs.Start();
    }

}
