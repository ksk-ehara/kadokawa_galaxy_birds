﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sluFPSchecker : MonoBehaviour
{
    //********************
    //  FPSを測って表示
    //      シーン上のオブジェクトにつけるだけでいい
    //********************
    
    public static bool viewFPS = true;  //これでオンオフきりかえ

    private float _interval = 0.5f; //fps計測のインターバル
    private float _elapsedTime = 0; //前回計測からの経過時間
    private float _elapsedFrame = 0;
    private float _fps = 0;

    private void Update()
    {
        if (!viewFPS) return;

        _elapsedTime += Time.deltaTime;
        _elapsedFrame++;

        if (_elapsedTime < _interval) return;

        _fps = _elapsedFrame / _elapsedTime;
        _elapsedTime = _elapsedFrame = 0;
    }

    Rect windowRect = new Rect(10, 20, 90, 20);
    private void OnGUI()
    {
        if (!viewFPS) return;
        windowRect = GUILayout.Window(0, windowRect, contents, "");
    }

    private void contents(int windowID)
    {
        GUILayout.Label("FPS: " + _fps.ToString("f2"));
    }
}
