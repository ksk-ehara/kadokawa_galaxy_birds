﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Drawing;

public class sluImgToTex : MonoBehaviour
{
    //// 外部からパスで画像を読み込んでテクスチャにするスクリプト

    public static Texture2D Get(string path)
    {
        Texture2D tex = default;
        //int width, height;
        byte[] bytes;

        /// バイト配列にする
        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))   //ファイルを読み込み
        {
            using (BinaryReader br = new BinaryReader(fs))  //そのファイルストリームをセット
                bytes = br.ReadBytes((int)br.BaseStream.Length);    //バイト配列を読みこみ
        } 
        

        /// 画像のサイズを取得
        string ext = path.Split('.')[path.Split('.').Length - 1].ToLower(); //拡張子


        /// テクスチャに変換



        return tex;
    }

}
