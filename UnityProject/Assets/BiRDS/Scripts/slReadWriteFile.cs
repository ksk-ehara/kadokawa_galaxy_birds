﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine;

//****************************************
// ファイル読み書きクラス
//****************************************
//  .txtや.csvを読みこんでリストに返したり
//  逆にリストを与えればそれで上書きするクラス


public class slReadWriteFile : MonoBehaviour
{
    // コンストラクタ
    public slReadWriteFile(){}

    //// パスを入力するとファイルを読み込んで1行ごとのリストにして返す
    public List<string> Read(string filePath)
    {
        var rtnList = new List<string>();
        var fi = new FileInfo(filePath);

        try
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)){ //using: 処理が終わったら()内をリソース解放
                using (StreamReader sr = new StreamReader(fs, Encoding.UTF8)){
                    while (sr.Peek() >= 0)
                        rtnList.Add(sr.ReadLine());
                }
            }
        }
        catch (Exception e) { Console.WriteLine("[ERROR] Exception: " + e); }

        return rtnList;
    }





    //// パスとリストか配列を指定して上書き（ファイルがなかったら作成）
    public void Write(string filePath, List<string> content)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8)) //using: 処理が終わったら()内をリソース解放
            {
                for(int i = 0; i < content.Count; i++)
                    sw.WriteLine(content[i]);
            }
        }
        catch (Exception e) { Console.WriteLine("[ERROR] Exception: " + e); }
    }
    public void Write(string filePath, string[] content)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8))
            {
                for (int i = 0; i < content.Length; i++)
                    sw.WriteLine(content[i]);
            }
        }
        catch (Exception e) { Console.WriteLine("[ERROR] Exception: " + e); }
    }







    //// csv読み込み
    public static string[,] ReadCsv(string filePath)
    {
        var list = new List<string[]>();
        var fi = new FileInfo(filePath);
        try
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) { 
                using (StreamReader sr = new StreamReader(fs, Encoding.UTF8)) {
                    Debug.Log(sr.Peek());
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        list.Add(line.Split(new char[] { ',' }, StringSplitOptions.None));
                    }
                }
            }
        }
        catch (IOException e) { 
            Debug.LogError("[ERROR] Exception: " + e);
            return null;
        }
        var rtnAry = new string[list.Count, list[0].Length];
        for(int i = 0; i < list.Count; i++)
            for (int j = 0; j < list[0].Length; j++)
                rtnAry[i, j] = list[i][j];

        return rtnAry;
    }





    //// csv書き込み
    public void WriteCsv(string filePath, string[,] content)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8)) //using: 処理が終わったら()内をリソース解放
            {
                for (int i = 0; i < content.GetLength(0); i++)
                {
                    string line = "";
                    for(int j = 0; j < content.GetLength(1); j++)
                    {
                        line += content[i, j];
                        if (j < content.GetLength(1) - 1) line += ",";  //行の最後尾以外はコンマ追加
                    }
                        sw.WriteLine(line);
                }
            }
        }
        catch (Exception e) { Console.WriteLine("[ERROR] Exception: " + e); }
    }



}
