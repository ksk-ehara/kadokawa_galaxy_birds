﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine;
using WebSocketSharp;
using Quobject.EngineIoClientDotNet.Modules;

public class LoadData : MonoBehaviour
{
    // 一日に一回データ更新用
    // 全部のデータを同時に読みこみ、string-string[]の連想配列に直す

    public Dictionary<string, string[]> Data { get; private set; } //読み取り専用 キャラクターとフレーズリストが対になったデータ
    public bool LoadEnd { get; private set; } = false;

    private GameObject alert = default;
    private Text alertText;

    private string _dataPath;

    private void Awake()
    {
        alert = GameObject.Find("ErrortText");
        alertText = alert.GetComponent<Text>();
    }


    public void Load(string Path)
    {
        LoadEnd = false;
        List<string> allLines = new List<string>();
        _dataPath = Path;

        try
        {
            using (FileStream fs = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))//using: 処理が終わったら()内をリソース解放
            { 
                using (StreamReader sr = new StreamReader(fs, System.Text.Encoding.UTF8))
                {
                    while (sr.Peek() >= 0)
                        allLines.Add(sr.ReadLine());
                }
            }
        }
        catch (Exception e) { Debug.Log("[ERROR] Exception: " + e); }


        if (allLines.Count == 0)
        {
            GenerateGroups.output.Log("[ERROR] --- データの読み込みに失敗しました " + _dataPath);
            alertText.text = "- - - [ERROR] - - -\n" + _dataPath + "が正しく読みこめません。\nパスが正しいか、別のソフトで開かれていないかを確認してください。";
            alertText.enabled = true;
            GenerateGroups.hasError = true;
        }
        else
        {
            Dictionary<string, string[]> data_tmp = new Dictionary<string, string[]>();
            int row = 0;    //何行目を処理しているか
            foreach (string line in allLines)
            {
                row++;
                string[] l = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                if (l.Length < 2)
                {
                    GenerateGroups.output.Log($"Error データの読み込みに失敗しました--- {_dataPath} ,{row}行目"); //記述がおかしい行はエラー出して飛ばす
                    continue;
                }

                string[] phraseAry = new string[50];
                string[] splitAry = l[1].Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries); //コンマで分けて配列に
                int phraseNum = splitAry.Length;
                for (int i=0; i<50; i++)
                {
                    if (i < phraseNum) phraseAry[i] = splitAry[i];
                    else phraseAry[i] = ""; //フレーズが50より少ない部分は空白で埋める
                }
                data_tmp.Add(l[0], phraseAry);
            }
            if(data_tmp.Count != 10)
            {
                GenerateGroups.output.Log("[ERROR] --- コンテンツの数とデータの数が一致していません " + _dataPath);
                alertText.text = "- - - [ERROR] - - -\n" + _dataPath + "コンテンツの数とデータの数が一致していません。\ndata.txtの中を記述を確認してください。";
                GenerateGroups.hasError = true;
                return;
            }

            Data = data_tmp;
            LoadEnd = true;
            GenerateGroups.output.Log($"{data_tmp.Count}データ読み込み完了 --- {Path}");
        }
    }



    //// グループ一つ更新してデータを外部に上書き
    public void Write(string GroupName, string[] PhraseAry)
    {
        Data[GroupName] = PhraseAry;

        using (StreamWriter writer = new StreamWriter(_dataPath, false, System.Text.Encoding.GetEncoding("utf-8")))
        {
            foreach (KeyValuePair<string, string[]> item in Data)
            { 
                string line = item.Key + "=";
                line += string.Join(",", item.Value);
                writer.WriteLine(line);
            }
        }
    }
}
