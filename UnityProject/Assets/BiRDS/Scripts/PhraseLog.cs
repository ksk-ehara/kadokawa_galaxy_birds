﻿using System;
using System.IO;
using UnityEngine;

public class PhraseLog : MonoBehaviour
{
    private string _fileName = "phrase_202010.txt";
    private string _lastDate = "202009";

    private string _pathA;
    private string _pathB;
        
    // コンストラクタ
    public PhraseLog(string PathA, string PathB = "")
    {
        _pathA = PathA;
        _pathB = PathB;
    }


    public void LogPhase(string Phrase)
    {
        if (DateTime.Today.ToString("yyyyMM") != _lastDate) _lastDate = DateTime.Today.ToString("yyyyMM");
        _fileName = $"phraseLog_{_lastDate}.txt";

        using (var fs = new FileStream(_pathA + _fileName, FileMode.Append, FileAccess.Write))
        {
            using (var sw = new StreamWriter(fs))
            {
                sw.WriteLine(Phrase);
            }
        }

        if (_pathB == "") return;

        using (var fs = new FileStream(_pathB + _fileName, FileMode.Append, FileAccess.Write))
        {
            using (var sw = new StreamWriter(fs))
            {
                sw.WriteLine(Phrase);
            }
        }
    }


}
