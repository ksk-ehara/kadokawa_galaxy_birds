﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ZXing.QrCode.Internal;

public class GenerateGroups : MonoBehaviour
{
    // メインスクリプト
    // - - - - - - - - - -
    //  各グループを生成
    //  モードの指示
    //  カメラ操作
    //  フレーズの受け取りと追加アニメーション
    //  基本的にデータはどちらもリアルタイム用のものを参照する
    //  フレーズデータはレンダリング用のものも更新されていく
    // 

    //// パス関連
    public static string dataDirPath_Rt = "";  //リアルタイム用PCのデータフォルダパス
    public static string dataDirPath_Rd = "";  //レンダリング用PCのデータフォルダパス
    private string _dataDir = @"KADOKAWA_data\";     //データフォルダ名
    private string _startuptxt = @"C:\Software\startup.txt";    //ソフトウェアフォルダパス

    private string _IP_PC1, _IP_PC2, _IP_PC3;   //本番機３台のIP
    private string _IP_dev_Rt = @"\\192.168.60.232\";     //開発環境時のリアルタイム用データフォルダを置いている場所
    private string _IP_dev_Rd = @"C:\Users\ksk-ehara\_mydata\1_work\202007_KadokawaBiRDS\unity\KADOKAWA_data_sub\";     //開発環境時のレンダリング用データフォルダを置いている場所


    //// アタッチするもの
    [SerializeField] private GameObject MainCam = default;
    [SerializeField] private GameObject GroupPrefab = default;
    [SerializeField] private GameObject AddPhrase = default;
    [SerializeField] private GameObject ErrorText = default;
    [SerializeField] private GameObject PhraseNgAlert = default;
    [SerializeField] private GameObject Model = default;
    [SerializeField] private TextMesh FrameNum = default;
    [SerializeField] private TMP_FontAsset Font = default;
    


    //// ログ書きだし
    public static sluOutput_Log output;

    //// 起動モード
    public static int mode = -1;    //0:リアルタイム、1:レンダリング、-1:起動しない
    private int[] _modes;
    public static bool hasError = false;
    private int _interval = 0;

    //// コンフィグ関連
    public static slConfig cnfg;
    private Color[] _colors;


    //// フレーズデータ関連
    private LoadData _ld;
    private LoadData _ldSub;
    private string[] _charaNames;   //キャラ名とIDの対応表
    private List<ARUserData> _addDataList;
    private bool _addNext = true;
    private PhraseLog _pl;


    //// フレーズグループの管理
    private GameObject[] _groupAry;


    //// NGリスト関連
    private string[] _ngList;
    private TextMesh _ngText;
    private string _alertText;

    //// フレーズモード関連
    public static int modeCount = 2; //モードの種類
    public static int currentMode = 0; //現在のモード
    private float _modeDelta = 0; //現在のモードの経過時間


    //// カメラ関連
    private Vector3 _camDefaultPos;
    private Vector3 _camStartPos;
    private Vector3 _camGoalPos;
    private Vector3 _moveStartLook = new Vector3(0, 0, 0);
    private Vector3 _moveGoalLook;
    public static Vector3 camDirection = new Vector3(0, 0, 1000);   //ビルボードに使用
    private int _camCurrentTarget = -1;
    private int _camNextTarget = -1;
    public static bool camMoving = false;
    private float _pastTime = 0; //カメラ移動の経過時間
    private int _camPastFrame = 0; //カメラ移動の経過時間
    private int _camMoveFrame; //カメラ移動に要するフレーム


    //// アニメーション関連
    private Animator _addAnim;
    private TextMesh _addMesh;
    private Transform _modelTf;


    //// レンダリング関連
    private RenderMovie _rm;
    private int _pastFrame = -90;
    private int _intervalCamMove;


    //// 動画配付
    private SendMovie _sm;
    private bool _finishRender = false;


    //// デモモード
    public static bool demoMode;



    void Awake()
    {
        //// 解像度固定
        Screen.SetResolution(1920, 1080, true);

        //// クォリティ設定(万が一PCで別の設定の記録が残っていても上書きできる)
        QualitySettings.SetQualityLevel(5);

        //// ログ表示とログの外部書き出しを同時に行うクラス
        output = gameObject.AddComponent<sluOutput_Log>();
        output.Log( $"\n\n- - - - - - - - - - - - - - - - - - - - － － － － － － － － － － " +
                    $"\n[{DateTime.Now}] アプリ起動");

        //// startup.txtを読みこんでIP指定、自身のIPと比較して起動設定
        setIPandMode();

        //// PCのモードを認識しパスを設定
        SetDirPath();

        //// config読み込み
        cnfg = new slConfig(dataDirPath_Rt + "config.txt");
    }




    //// startup.txtと自身のIPと比較して起動設定
    private void setIPandMode()
    {
        List<string> myIP = new List<string>();
        try
        {
            //ホスト名を取得
            string hostname = System.Net.Dns.GetHostName();

            //ホスト名からIPアドレスを取得
            System.Net.IPAddress[] addr_arr = System.Net.Dns.GetHostAddresses(hostname);

            //探す
            foreach (System.Net.IPAddress addr in addr_arr)
            {
                string addr_str = addr.ToString();
                output.Log("GetHostAddress = " + addr_str);

                //IPv4 && localhostでないなら自身のIPとして記録
                if (addr_str.IndexOf(".") > 0 && !addr_str.StartsWith("127."))
                {
                    myIP.Add(addr_str);
                    break;
                }
            }
        }
        catch (Exception)
        {
            output.Log("[ERROR] IP取得中にエラーが生じました");
            myIP = null;
        }

        //// テキストファイルから情報取得
        var _IPList = new List<string[]>();
        try
        {
            using (StreamReader sr = new StreamReader(_startuptxt))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line == "") continue;                   //空行はとばして次へ
                    if (line.Substring(0, 1) == "#") continue;  //コメントはとばして次へ

                    // =で分ける
                    string[] parts = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);  
                    
                    if (parts.Length == 2 && (parts[0].Split(new char[] { '.' }).Length == 4))
                    {
                        _IPList.Add(new string[] { parts[0], parts[1] }); //stringの配列に記録
                        if (myIP.Contains(parts[0])) mode = int.Parse(parts[1]);
                    }
                }
            }
            output.Log($"このPCは mode = {mode}");
        }
        catch
        {
            output.Log($"{_startuptxt}の読み込みに失敗しました");
            ErrorText.GetComponent<Text>().text = $"{_startuptxt}の読み込みに失敗しました";
            hasError = true;
        }


        if (mode == -1)
        {
            foreach (string IP in myIP)
            {
                output.Log($"IPアドレス{IP} は、startup.txtで起動する設定になっていません");
                ErrorText.GetComponent<Text>().text = $"IPアドレス{IP} は、startup.txtで起動する設定になっていません";
                hasError = true;
            }
        }


        // BiRDS1,2,3のパス指定
        _IP_PC1 = $@"\\{_IPList[0][0]}\";
        _IP_PC2 = $@"\\{_IPList[1][0]}\";
        _IP_PC3 = $@"\\{_IPList[2][0]}\";

        // BiRDS1,2,3がそれぞれ何のモードで起動するか記録
        _modes = new int[] { int.Parse(_IPList[0][1]), int.Parse(_IPList[1][1]), int.Parse(_IPList[2][1]) };
    }





    //// setIPandModeで読みこんだ起動モードに従って、リアルタイムとレンダリングのパスを設定する
    private void SetDirPath()
    {
        if (hasError) return;

        //// 開発PCのみ起動
        if (Directory.Exists(_IP_dev_Rt + _dataDir)) //開発環境でのテスト
        {
            dataDirPath_Rt = _IP_dev_Rt + _dataDir;
            dataDirPath_Rd = _IP_dev_Rd + _dataDir;
            output.Log("開発用PCで特殊起動中（単体起動かつバックアップあり）");
            return;
        }



        string[] ipList = { _IP_PC1, _IP_PC2, _IP_PC3 };

        for (int i = 0; i < _modes.Length; i++)
        {
            switch (_modes[i])
            {
                case 0:
                    dataDirPath_Rt = ipList[i] + _dataDir;
                    output.Log($"BiRDS{i + 1}(IP:{ipList[i]})はリアルタイムモードで起動");
                    break;
                case 1:
                    dataDirPath_Rd = ipList[i] + _dataDir;
                    output.Log($"BiRDS{i + 1}(IP:{ipList[i]})はレンダリングモードで起動");
                    break;
                case -1:
                    output.Log($"BiRDS{i + 1}(IP:{ipList[i]})は起動しない設定になっています。");
                    break;
            }
        }



        if (dataDirPath_Rt == "")
        {
            output.Log("リアルタイム用のPCが設定されていません。C:Software\\startup.txtを確認してください");
            ErrorText.GetComponent<Text>().text = "リアルタイム用のPCが設定されていません\nC:Software\\startup.txtを確認してください";
            hasError = true;
        }
        if (dataDirPath_Rd == "")
        {
            output.Log("レンダリング用のPCが設定されていません。C:Software\\startup.txtを確認してください");
            ErrorText.GetComponent<Text>().text = "レンダリング用のPCが設定されていません\nC:Software\\startup.txtを確認してください";
            hasError = true;
        }
        if (!Directory.Exists(dataDirPath_Rt))
        {
            output.Log($"{dataDirPath_Rt}にアクセス出来ません");
            ErrorText.GetComponent<Text>().text = $"{dataDirPath_Rt}にアクセス出来ません";
            hasError = true;
        }
    }





    void Start()
    {
        if (hasError) return;  //起動不可能なエラーが生じた際は処理全てスキップ

        Cursor.visible = false;

        //// configから色々設定
        _camMoveFrame = (int)Math.Round(cnfg.cf_camMoveDuration * cnfg.cf_fps);
        demoMode = cnfg.cf_demoMode;

        /// fps固定設定
        QualitySettings.vSyncCount = 0;     //垂直同期(モニタのリフレッシュレートとの同期)を無効化
        Application.targetFrameRate = cnfg.cf_fps;

        /// fps表示オフ
        sluFPSchecker.viewFPS = false;

        //// データ読み込み
        _ld = gameObject.AddComponent<LoadData>();    //独自クラスのLoadDataを取得
        _ld.Load(dataDirPath_Rt + "data.txt");
        _ldSub = gameObject.AddComponent<LoadData>();
        _ldSub.Load(dataDirPath_Rd + "data.txt");

        int dataNum = _ld.Data.Count;

        _charaNames = new string[dataNum];
        _addDataList = new List<ARUserData>();

        _pl = new PhraseLog(dataDirPath_Rt+ "Log\\phrase\\", dataDirPath_Rd+ "Log\\phrase\\");


        // 色設定
        _colors = new Color[dataNum];
        _colors[0] = new Color(cnfg.cf_colorA[0], cnfg.cf_colorA[1], cnfg.cf_colorA[2]);
        _colors[1] = new Color(cnfg.cf_colorB[0], cnfg.cf_colorB[1], cnfg.cf_colorB[2]);
        _colors[2] = new Color(cnfg.cf_colorC[0], cnfg.cf_colorC[1], cnfg.cf_colorC[2]);
        _colors[3] = new Color(cnfg.cf_colorD[0], cnfg.cf_colorD[1], cnfg.cf_colorD[2]);
        _colors[4] = new Color(cnfg.cf_colorE[0], cnfg.cf_colorE[1], cnfg.cf_colorE[2]);
        _colors[5] = new Color(cnfg.cf_colorF[0], cnfg.cf_colorF[1], cnfg.cf_colorF[2]);
        _colors[6] = new Color(cnfg.cf_colorG[0], cnfg.cf_colorG[1], cnfg.cf_colorG[2]);
        _colors[7] = new Color(cnfg.cf_colorH[0], cnfg.cf_colorH[1], cnfg.cf_colorH[2]);
        _colors[8] = new Color(cnfg.cf_colorI[0], cnfg.cf_colorI[1], cnfg.cf_colorI[2]);
        _colors[9] = new Color(cnfg.cf_colorJ[0], cnfg.cf_colorJ[1], cnfg.cf_colorJ[2]);


        // 空間モデルを回す準備
        _modelTf = Model.transform;


        // カメラの初期位置記録
        _camDefaultPos = MainCam.transform.position;


        // デモモード
        if (!demoMode) FrameNum.gameObject.SetActive(false);    //フレーム表示オフ



        // フォントデータリセット
        Font.ClearFontAssetData();


        //// 各Group生成
        // itemの個数GenerateOneGroupをクローン生成して値わたす
        _groupAry = new GameObject[dataNum];
        int count = 0;
        foreach (KeyValuePair<string, string[]> item in _ld.Data)
        {
            float x = 0, y = 0, z = 0;
            y = cnfg.cf_groupsHeight / 2 - cnfg.cf_groupsHeight / (dataNum - 1) * count; // 一番上 - 下がり幅 * 何個目
            float theta = (float)(count) / (dataNum - 1) * 2 * Mathf.PI * cnfg.cf_groupsSpiral + Mathf.PI * 2 * 0.33f; // 全体の何割目か * 360° *　最終的に何周するか + スタート位置をずらす
            x = Mathf.Sin(theta) * cnfg.cf_groupsRadius;
            z = Mathf.Cos(theta) * cnfg.cf_groupsRadius;

            _groupAry[count] = Instantiate(GroupPrefab, new Vector3(x, y, z), transform.rotation, transform);
            _groupAry[count].GetComponent<GenerateOneGroup>().init(count, item.Value, _colors[count]);

            _charaNames[count] = item.Key;  //ついでにIDキャラ名対応表も埋める

            count++;
        }




        // 以下はリアルタイムモードかレンダリングモードかで分岐
        if (mode == 0)
        {
            //// リアルタイムモード ////


            _ngText = PhraseNgAlert.GetComponent<TextMesh>();
            _alertText = _ngText.text;

            //// NGワードリスト読み込み
            string[,] tmpList;
            tmpList = slReadWriteFile.ReadCsv(dataDirPath_Rt + "ngList.csv");


            _ngList = new string[tmpList.GetLength(0)];
            for (int i = 0; i < _ngList.Length; i++)
            {
                _ngList[i] = tmpList[i, 0]; //csvは二重配列で帰ってくるのでそれを配列に直す
            }
            PhraseNgAlert.SetActive(false);


            //// フレーズ追加アニメーション準備
            _addAnim = AddPhrase.GetComponent<Animator>();
            _addMesh = _addAnim.GetComponent<TextMesh>();
            
            output.Log("リアルタイムモードで起動");
        }
        else
        {
            //// レンダリングモード ////
            //// インスタンス生成とプロパティ設定
            _rm = gameObject.GetComponent<RenderMovie>();
            _rm.width = cnfg.cf_mvWidth;
            _rm.height = cnfg.cf_mvHeight;
            _rm.eachMvFrame = (int)Math.Round(cnfg.cf_EachMvLength * cnfg.cf_fps);
            _rm.camMoveFrame = _camMoveFrame;
            _rm.fps = cnfg.cf_fps;
            _rm.Init(); //プロパティを踏まえて初期設定


            _sm = gameObject.AddComponent<SendMovie>();
            _sm.Init();

            ErrorText.gameObject.transform.root.GetComponentInChildren<SpriteRenderer>().gameObject.SetActive(false);

            output.Log("レンダリングモードで起動");
        }
        _intervalCamMove = (int)Math.Round((cnfg.cf_EachMvLength + cnfg.cf_camMoveDuration) * cnfg.cf_fps);   // カメラを動かす間隔
    }




    void Update()
    {
        //// Escキーで終了
        if (Input.GetKeyDown(KeyCode.Escape))
            CloseApp();



        if (hasError) return;  //エラー時は以下の処理スキップ


        //// cキーでコンフィグモード
        if (Input.GetKeyDown(KeyCode.C))
        {
            sluFPSchecker.viewFPS = sluFPSchecker.viewFPS ? false : true;
        }


        //// 空間モデルをゆっくり回す
        _modelTf.Rotate(0, 0.02f, 0);



        //// キーボードでカメラ制御
        if (Input.GetKey(KeyCode.Q)) startCamMove(-1);
        if (Input.GetKey(KeyCode.Alpha1)) startCamMove(0);
        if (Input.GetKey(KeyCode.Alpha2)) startCamMove(1);
        if (Input.GetKey(KeyCode.Alpha3)) startCamMove(2);
        if (Input.GetKey(KeyCode.Alpha4)) startCamMove(3);
        if (Input.GetKey(KeyCode.Alpha5)) startCamMove(4);
        if (Input.GetKey(KeyCode.Alpha6)) startCamMove(5);
        if (Input.GetKey(KeyCode.Alpha7)) startCamMove(6);
        if (Input.GetKey(KeyCode.Alpha8)) startCamMove(7);
        if (Input.GetKey(KeyCode.Alpha9)) startCamMove(8);
        if (Input.GetKey(KeyCode.Alpha0)) startCamMove(9);




        //// 表示モード制御
        //_modeDelta += Time.deltaTime;
        _modeDelta += 1f/(float)cnfg.cf_fps;
        if (_modeDelta >= cnfg.cf_modeSpan)
        {
            currentMode = (currentMode < modeCount - 1) ? currentMode + 1 : 0;  //モードを順に切り替え
            _modeDelta = 0;
        }



        //// カメラ移動処理
        if (camMoving)  //移動中フラグがたっているか
        {
            //経過時間、スタートベクトル、移動ベクトル、移動に欠ける時間を入力して現在地を求める
            MainCam.transform.position = MyEasingVector.easeOutCirc(_camPastFrame, _camStartPos, _camGoalPos - _camStartPos, _camMoveFrame);
            MainCam.transform.LookAt(MyEasingVector.easeOutCirc(_camPastFrame, _moveStartLook, _moveGoalLook - _moveStartLook, _camMoveFrame));
            camDirection = new Vector3(MainCam.transform.position.x, 0, MainCam.transform.position.z) * -1000;

            for (int i = 0; i < _groupAry.Length; i++)
            {
                _groupAry[i].transform.LookAt(camDirection);
            }


            if (_camPastFrame < cnfg.cf_camMoveDuration *cnfg.cf_fps)
            {
                _camPastFrame++;
            }
            else
            {
                /// 移動終了
                _camPastFrame = 0;
                camMoving = false;
                _camCurrentTarget = _camNextTarget;
                _moveStartLook = _moveGoalLook;
            }
        }


        /// リアルタイムモード
        if (mode == 0)
        {
            //// 5分に一度時刻を見て、22時以降なら1分後にアプリ終了
            if (!demoMode)
            {
                if (_interval>30)
                {
                    DateTime dt = DateTime.Now;
                    if (dt.Hour >= 22 && _addDataList.Count == 0)
                    {
                        ErrorText.GetComponent<Text>().text = $"22時を過ぎたため、まもなくアプリを終了します。";
                        output.Log($"{dt:HH:mm:ss} 22時を過ぎたため、まもなくアプリを終了します。");
                        _interval = -1800;  //終了までのカウントダウンフレーム
                    }
                    else _interval = 0; //22時以降じゃないならまた5分後に時刻確認
                }
                else if(_interval < 0 && _interval > -2)
                {
                    CloseApp();
                }
                _interval++;
            }


                if (_addNext)　// 追加アニメーション中じゃない
            {
                //// 追加待ちフレーズがあれば追加処理
                if (_addDataList.Count>0) addPhrase(this);
                //// なければカメラループ
                else LoopCam();
            }
        }
    }



    //private void OnRenderImage()
    private void LateUpdate()
    {
        if (hasError) return;  //エラー時は以下の処理スキップ

        //// レンダリングモード
        if (mode == 1 && !_finishRender)
        {
            if(demoMode)
                FrameNum.text = _pastFrame.ToString();

            if (_pastFrame > -1) //一秒後にレンダリング開始
            {
                //// レンダリング
                _rm.startRender();

                if (_rm.finished == 0)
                {
                    //// カメラ移動
                    if (_pastFrame % _intervalCamMove == 0 && _pastFrame / _intervalCamMove < 11)  //インターバルごとにカメラ移動
                    {
                        int nextAngle = (_camCurrentTarget != 9) ? _camCurrentTarget + 1 : -1;
                        startCamMove(nextAngle);
                    }
                }
                else　if(_rm.finished == 1)//movie完成後一回だけこの分岐に入る
                {
                    Debug.Log("finish1");
                    _finishRender = true;
                    _sm.StartSendMv();
                }
                else if(_rm.finished == -1)//レンダリング失敗の場合はこのまま終わる
                {
                    Debug.Log("finish-1");
                    _finishRender = true;
                    CloseApp();
                }
            }
            _pastFrame++;
        }
    }






    //// カメラの移動開始処理
    private void startCamMove(int num)    // -1:全体、それ以外:番号に応じたグループ
    {
        if (!camMoving && _camCurrentTarget != num && _pastTime == 0)
        {
            //Debug.Log("Move to " + num);
            camMoving = true;
            _camNextTarget = num;
            _camStartPos = MainCam.transform.position;
            if (num == -1)
            {
                _camGoalPos = _camDefaultPos;
                _moveGoalLook = new Vector3(0, -3, 0);
            }
            else
            {
                ///球体
                //Vector3 pos = _groupAry[num].transform.position;
                //_moveGoalPos = new Vector3(pos.x, pos.y, pos.z - 25);
                //_moveGoalLook = new Vector3(0, 0, 0);

                ///円柱
                Vector3 pos = _groupAry[num].transform.position;
                float r = cnfg.cf_groupsRadius;
                _camGoalPos = new Vector3(pos.x / r * (r + 30), pos.y, pos.z / r * (r + 30));  //pos.x=sin(theta)*rなので
                _moveGoalLook = new Vector3(0, pos.y-4, 0);
            }
        }
    }





    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
    // ライブモード用のメソッド
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  

    //// メッセージ受信時に呼び出されるメソッド
    public void GetMessageFromUser(ARUserData userData)
    {
        string msg = userData.Message;
        string date = DateTime.Now.ToString("MM/dd");
        string time = DateTime.Now.ToString("HH:mm:ss");
        string ngMarker = "";

        output.Log($"フレーズ受信 {date} {time} {userData.SelectNo} : {userData.Message}");

        //// NGかどうかチェック
        bool detection = false; //NGワード検出フラグ
        if(msg.Length == 0) detection = true;  //中身が空
        if (msg.Replace(" ", "").Length + msg.Replace("　", "").Length <= msg.Length) detection = true;  //空白のみ
        string phrase = UtilWordConverter.ConvertToKatakana(msg.ToUpper());
        Debug.Log("phrase" + phrase);
        for (int i = 0; i < _ngList.Length; i++)
        {
            if (phrase.Contains(_ngList[i])) detection = true;
        }


        //// NGかどうかで処理
        if (!detection)
        {
            /// フレーズを追加待ちリストに加える
            _addDataList.Add(userData.UtilCopy());
            output.Log($"フレーズリスト追加 --- {msg}");
        }
        else
        {
            /// NG警告を画面に出してワード蓄積の際のマーカーON
            AlertNG(this, time);
            output.Log($"NG警告 --- {msg}");
            ngMarker = "[NG]";
        }

        // フレーズデータを蓄積
        _pl.LogPhase($"{date} {time} {ngMarker}{userData.Message}");
    }



    //##### 201030-add
    //#### サーバーとの接続が成功した時
    public void SuccessServerConnection()
    {
        ErrorText.GetComponent<Text>().text = "";
    }

    //#### サーバーとの接続が失敗した時    
    public void ErrorServerConnection()
    {
        ErrorText.GetComponent<Text>().text = "サーバーとの接続が出来ません。";

    }
    //##### 201030-end





    static async void AlertNG(GenerateGroups gg, string time)
    {
        // アラートを指定秒数表示
        gg.PhraseNgAlert.SetActive(true);
        gg._ngText.text = $"[{time} に送信されたメッセージ]\n{gg._alertText}";
        await Task.Delay(8000);
        gg.PhraseNgAlert.SetActive(false);
    }




    static async void addPhrase(GenerateGroups gg) //秒数待ちするためにstatic async
    {
        gg._addNext = false;    //追加処理中なのでフラグをおろす

        ARUserData data = gg._addDataList[0];   //追加待ちリストの先頭を取得

        int charaID = int.Parse(data.SelectNo);

        //// フレーズをアニメーション用メッシュに反映
        gg._addMesh.color = gg._colors[charaID];
        gg._addMesh.text = data.Message;

        await Task.Delay(2000);   //カメラの移動前に待機
        
        // カメラ移動
        gg.startCamMove(charaID);
        await Task.Delay((int)cnfg.cf_camMoveDuration * 1000 + 2000);   //カメラの移動にかかる時間待機、移動後2秒待機

        // フレーズ追加アニメーション
        gg._addAnim.Play("AddPharase01");
        await Task.Delay(5000); //アニメーションにかかる時間待機

        //グループに反映
        GenerateOneGroup gog = gg._groupAry[charaID].GetComponent<GenerateOneGroup>();
        gog.updatePhrase(data.Message);
        await Task.Delay(2000); //追加後少し待機

        //データに反映
        gg._ld.Write(gg._charaNames[charaID], gog._phraseAry);
        if (dataDirPath_Rd != null) gg._ldSub.Write(gg._charaNames[charaID], gog._phraseAry);
       

        // 追加完了したのでリストの先頭から削除
        gg._addDataList.RemoveAt(0);

        // 次のフレーズ受け入れ態勢が整ったのでフラグをたてる
        gg._addNext = true;
        gg._pastFrame = 0;
    }




    private void LoopCam()
    {
        if (_pastFrame>0)  //インターバルごとにカメラ移動
        {
            if (_pastFrame%_intervalCamMove==0)  //インターバルごとにカメラ移動
            {
                int nextAngle = (_camCurrentTarget != 9) ? _camCurrentTarget + 1 : -1;
                startCamMove(nextAngle);
            }
        }
        _pastFrame++;
    }







    //// アプリ終了処理
    public static void CloseApp()
    {
        output.Log($"\n{DateTime.Now} アプリ終了" +
                   $"\n");

        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

}
