﻿using System.IO;
using System;

public class slIO_plus
{
    // - - - - - - - - - - 
    // FileIOを補強するクラス



    // - - - - - - - - - - 
    // ディレクトリの中身だけ全削除
    // ※※※ Directory.Delete(path, true)で全部消した後に同じDirectory作ればいい話ではある
    public static void ResetDirectory(string Path)
    {
        string[] filePaths = null;
        try
        {
            filePaths = Directory.GetFiles(Path);
        }
        catch(Exception ex) when (ex is DirectoryNotFoundException || ex is ArgumentException)
        {
            Console.WriteLine($"ディレクトリ{Path} は存在しません");
            return;
        }


        if (filePaths == null || filePaths.Length < 1)
        {
            Console.WriteLine($"{Path} はすでに空です");
            return;
        }


        string current_file = "";
        try
        {
            foreach (string file in filePaths)
            {
                current_file = file;
                File.Delete(file);
            }
        }
        catch
        {
            Console.WriteLine($"{current_file} の削除時にエラーが生じました");
        }
    }





    // - - - - - - - - - - 
    // ディレクトリ内のファイル全てを移動
    public static void MoveAllFiles(string SourceDir, string DestinationDir, bool Overwrite=true)
    {
        string[] filePaths = null;
        try
        {
            filePaths = Directory.GetFileSystemEntries(SourceDir);
        }
        catch (Exception ex) when (ex is DirectoryNotFoundException || ex is ArgumentException)
        {
            Console.WriteLine($"ディレクトリ{SourceDir} は存在しません");
            return;
        }


        if (filePaths == null || filePaths.Length < 1)
        {
            Console.WriteLine($"移動元ディレクトリ{SourceDir} は空です");
            return;
        }


        //// ファイルごとに移動
        string current_file = "";
        try
        {
            foreach (string file in filePaths)
            {
                current_file = file;
                string fileName = null;

                if (File.GetAttributes(file).HasFlag(FileAttributes.Directory)) //directoryかfileか判断
                {
                    fileName = Path.GetDirectoryName(file);
                    if (Directory.Exists(DestinationDir + fileName))
                    {
                        if (Overwrite)
                        {
                            Directory.Delete(DestinationDir + fileName, true);
                        }
                        else
                        {
                            Console.WriteLine($"{DestinationDir + fileName}は既に存在します。");
                            continue;
                        }
                    }
                    Directory.Move(file, DestinationDir + fileName);
                }
                else
                {
                    fileName = Path.GetFileName(file);
                    if (File.Exists(DestinationDir + fileName))
                    {
                        if (Overwrite)
                        {
                            File.Delete(DestinationDir + fileName);
                        }
                        else
                        {
                            Console.WriteLine($"{DestinationDir + fileName}は既に存在します。");
                            continue;
                        }
                    }
                    File.Move(file, DestinationDir + fileName);
                }
            }
        }
        catch
        {
            Console.WriteLine($"{current_file}の移動中にエラーが生じました");
        }
    }




    // - - - - - - - - - - 
    // ディレクトリ内のファイル全てをコピー
    public static void CopyAllFiles(string SourceDir, string DestinationDir, bool Overwrite = true)
    {
        string[] filePaths = null;
        try
        {
            filePaths = Directory.GetFileSystemEntries(SourceDir);
        }
        catch (Exception ex) when (ex is DirectoryNotFoundException || ex is ArgumentException)
        {
            Console.WriteLine($"ディレクトリ{SourceDir} は存在しません");
            return;
        }


        if (filePaths == null || filePaths.Length < 1)
        {
            Console.WriteLine($"移動元ディレクトリ{SourceDir} は空です");
            return;
        }


        //// ファイルごとに移動
        string current_file = "";
        try
        {
            foreach (string file in filePaths)
            {
                current_file = file;
                string fileName = null;

                if (File.GetAttributes(file).HasFlag(FileAttributes.Directory)) //directoryかfileか判断
                {
                    fileName = Path.GetDirectoryName(file);
                    if (Directory.Exists(DestinationDir + fileName))
                    {
                        if (Overwrite)
                        {
                            Directory.Delete(DestinationDir + fileName, true);
                        }
                        else
                        {
                            Console.WriteLine($"{DestinationDir + fileName}は既に存在します。");
                            continue;
                        }
                    }
                    Directory.CreateDirectory(DestinationDir + fileName);
                }
                else
                {
                    fileName = Path.GetFileName(file);
                    if (File.Exists(DestinationDir + fileName))
                    {
                        if (Overwrite)
                        {
                            File.Delete(DestinationDir + fileName);
                        }
                        else
                        {
                            Console.WriteLine($"{DestinationDir + fileName}は既に存在します。");
                            continue;
                        }
                    }
                    File.Copy(file, DestinationDir + fileName);
                }
            }
        }
        catch
        {
            Console.WriteLine($"{current_file}の移動中にエラーが生じました");
        }
    }

}
