﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;


public class AppMainController : MonoBehaviour
{
    //##### 設定ファイル関連
    /*
    private string appSettingFolderDirPath = ""; //iniファイルがあるフォルダパス
    private string _SettingFolderName = "//SettingData//";
    private string _SettingFileName = "Setting.txt";
    private AppSoftEnv _appSoftEnv; //設定ファイルクラス
    */

    //// 外部データ関連
    private string configPath = "config.txt";

    private slConfig _cnfg;
    LoadData _ld;
    //hiro-end

    private string UnityAccessURL = "";
    private string UserPostingPageURL = "";

    public   AppNetSocketIOController progAppNetSocketIOController;



    public MakeQRAccessCodeURL progQRAccessCodeURL;

    //##### 以下ユーザー管理関連
    private ManageARUser _arUserManager;   
    public TextMeshPro progLogText;
    public TextMeshPro progInfoParam;

    public  GenerateGroups      progBiRDS;


    //201030-add
    //##### エラー処理関連
    private int _iTryServerConnectNum = 0;
    private int _iTryServerConnectMax = 60;
    //201030-end


    //######### GUI関連
    #region

    public bool GUIShowEnable = false;
    private int setWidth = 200;
    private int setHeight = 100;
    private Rect _ctrWinRect;
    private bool _ControlGUIShow = true;

    #endregion



    //#####################################################
    //################## Startメソッド ####################
    //#####################################################

    void Start()
    {
        //####
        //#### 設定ファイルの読み込み
        //####
        /* 
        //### UnityEditorで実行している時の設定ファイルパス
        if (Application.isEditor == true)
            this.appSettingFolderDirPath = Application.dataPath + this._SettingFolderName + this._SettingFileName;
        //### Exeで実行している時の設定ファイルパス
        else
            this.appSettingFolderDirPath = System.IO.Directory.GetCurrentDirectory() + this._SettingFolderName + this._SettingFileName;

        this._appSoftEnv = new AppSoftEnv();
        this._appSoftEnv.UtilLoadSettingFile(this.appSettingFolderDirPath);
        this._appSoftEnv.DebugPrint();
        */

        if (GenerateGroups.mode == 1) 
        { 
            transform.root.gameObject.SetActive(false);    //レンダーモードの時はNodeJSCoreObjectsごと非アクティブ
            return;
        }
        /// config読み込み
        configPath = GenerateGroups.dataDirPath_Rt + configPath;
        _cnfg = new slConfig(configPath);

        //####
        //#### ユーザー管理作成
        //####
        this._arUserManager = new ManageARUser();

        this._arUserManager.cbAddUser    += this._arUserManager_cbAddUser;
        this._arUserManager.cbDeleteUser += this._arUserManager_cbDeleteUser;
        this._arUserManager.cbAllClearUserData += this._arUserManager_cbAllClearUserData;
        this._arUserManager.cbEditUserData += this._arUserManager_cbEditUserData;

        //####
        //#### アクセスコード入りのQRコード生成クラスをセットアップ
        //####
        //#### hiro-add
        this.UserPostingPageURL = this._cnfg.cf_UserPostingPageURL;
        //#### hiro-end
        this.progQRAccessCodeURL.UtilSetBaseURL(this.UserPostingPageURL);


        //####
        //#### サーバー接続開始
        //####
        //#### hiro-add        
        this.UnityAccessURL = this._cnfg.cf_UnityAccessURL;
        //        this.progInfoParam.text = "Connection :: " + this.AccessURL;
        //#### hiro-end



        //201030-add
        this._iTryServerConnectMax = GenerateGroups.cnfg.cf_iTryServerConnectMax;


        this.progAppNetSocketIOController.UtilConnectNodeJSServer(this.UnityAccessURL,
            //#### 成功時
            () => {
                //##### サーバーへログイン
                Debug.Log(DateTime.Now.ToString("HH:mm:ss")+" 接続成功 :: " + this.UnityAccessURL);
                this.progAppNetSocketIOController.UtilLoginServer();
                this._iTryServerConnectNum = 0;
                this.progQRAccessCodeURL.UtilSetVisibleQRCodePlane(true);   //QRコード表示

                //#### BiRDsプログラム呼び出し                      
                this.progBiRDS.SuccessServerConnection();

            },
            //#### エラー時
            () => {
                Debug.LogError(DateTime.Now.ToString("HH:mm:ss") + " 接続失敗 :: " + this.UnityAccessURL + "  TryNum = " + this._iTryServerConnectNum);

                this._iTryServerConnectNum++;
                if (this._iTryServerConnectNum > this._iTryServerConnectMax)
                {
                    Debug.LogError("エラーダイアログ表示");
                    this.progQRAccessCodeURL.UtilSetVisibleQRCodePlane(false);   //QRコード非表示                    
                                                                                 //#### BiRDsプログラム呼び出し
                    this.progBiRDS.ErrorServerConnection();
                }
            }
         );
        //201030-end


        //#### ユーザー管理とURL発行オブジェクトをSocket.IOコントローラに渡す
        this.progAppNetSocketIOController.UtilSetup(this._arUserManager, this.progQRAccessCodeURL);

    }

    private void _arUserManager_cbAddUser(object sender, EventArgs e)
    {
        ARUserData userData = sender as ARUserData;
        //Debug.Log("新規ユーザー追加");
        //userData.UtilPrint();
        string log = "ID = " + userData.userID + "  :: NickName = 「" + userData.NickName + "」が参加";
        this._UpdateLogText(log);
    }

    //##### コールバック :: 管理マネージャーにユーザーが削除された時に呼び出される
    private void _arUserManager_cbDeleteUser(object sender, EventArgs e)
    {
        ARUserData deleteUserData = sender as ARUserData;
        //Debug.Log("ユーザー削除");
        //deleteUserData.UtilPrint();
        string log = "ID = " + deleteUserData.userID + "  :: NickName = 「" + deleteUserData.NickName + "」が脱退しました";
        this._UpdateLogText(log);
    }


    //##### コールバック :: 管理マネージャーですべてのユーザーが削除された時に呼び出される
    private void _arUserManager_cbAllClearUserData(object sender, EventArgs e)
    {
//        string log = "全ユーザーが削除されました。";
//        this._UpdateLogText(log);
    }


    //######　コールバック :: ユーザ情報が何か変更されたら
    private void _arUserManager_cbEditUserData(object sender, EventArgs e)
    {
        ARUserData editUuserData = sender as ARUserData;
        string log =  editUuserData.NickName + "が「" + editUuserData.Message + "」を投稿";
        //this._UpdateLogText(log);

        //#### BiRDsプログラム呼び出し
        this.progBiRDS.GetMessageFromUser(editUuserData );
    }

    private void _UpdateLogText(string setLogStr)
    {
        /* 
        string log = this.progLogText.text;
        string newLog = setLogStr + "\r\n"+ log;
        this.progLogText.text = newLog;
        */
    }


    // Update is called once per frame
   //#####################################################
    //################## Updateメソッド ####################
    //#####################################################
    void Update () 
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (this.GUIShowEnable == true)
                this.GUIShowEnable = false;
            else if (this.GUIShowEnable == false)
                this.GUIShowEnable = true;
        }
	}


        void OnGUI()
    {
    
        #region

        if (this.GUIShowEnable == true)
        {
            if (this._ControlGUIShow == false)
                return;

            this._ctrWinRect = GUILayout.Window(1, this._ctrWinRect, this._DoControlWindow, "");
        }
    }


    private void _DoControlWindow(int windowID)
    {

        GUILayout.BeginVertical("");
        //############
        if (GUILayout.Button("サーバーへの接続") == true)
        {
            this.progAppNetSocketIOController.UtilConnectNodeJSServer(this.UnityAccessURL,
                 //#### 成功時
                 () => {
                     this.progAppNetSocketIOController.UtilLoginServer();

                 },
                 //#### エラー時
                 () => {
                     Debug.LogError(DateTime.Now.ToString("HH:mm:ss") + " 接続失敗 :: " + this.UnityAccessURL);
                 }
          );
        }


        if (GUILayout.Button("サーバーと切断") == true)
        {
            this.progAppNetSocketIOController.UtilDisconnectNodeJSServer(null);
        }



        GUILayout.Label("------------------");
       

        GUILayout.EndVertical();

        GUI.DragWindow();

        #endregion
    }
}
