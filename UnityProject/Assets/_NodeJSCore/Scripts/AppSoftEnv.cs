﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AppSoftEnv
{
    //########### デフォルト値
    private string  _Def_UnityAccessURL = "127.0.0.1:8080";
    private string _Def_UserPostingPageURL = "https://127.0.0.1:10443/postingmsg/index.html";


    //########### 実際に使う値
    public string UnityAccessURL = "";
    public string UserPostingPageURL = "";


    //#############
    //############# コンストラクタ
    //#############
    public AppSoftEnv()
    {
        this.UtilSetDefaultValue();
    }


    //###### デフォルトの値に戻す
    public  void    UtilSetDefaultValue()
    {
        this.UnityAccessURL     = this._Def_UnityAccessURL;
        this.UserPostingPageURL = this._Def_UserPostingPageURL;
    }



    //################ 設定ファイルロードメソッド ##################
    public bool UtilLoadSettingFile(string setLoadFilePath)
    {
        if (File.Exists(setLoadFilePath) == false)
            return false;

        StreamReader fsReader = new StreamReader(setLoadFilePath);

        //ファイル内末尾までループ
        while (fsReader.Peek() >= 0)
        {
            string readStr = fsReader.ReadLine();  //1行ずつ読み込み読み込み

            //「=」で文字列分割
            string[] delimiter = new string[1];
            delimiter[0] = "=";
            string[] divStrArray = readStr.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            // コメント分、改行のみの行の場合はこれ以降処理しない。
            if (divStrArray.Length != 2)
                continue;


            if (divStrArray[0] == "UnityAccessURL")
                this.UnityAccessURL = divStrArray[1];

            if (divStrArray[0] == "UserPostingPageURL")
                this.UserPostingPageURL = divStrArray[1];


            /*
            //### Boolean値を変換＆格納
            if (divStrArray[0] == "ParamBool")
            {
                string getValue = divStrArray[1];
                if (getValue == "1" || getValue == "true")
                    this.ParamBool = true;
                else
                    this.ParamBool = false;
            }

            //### Int値を変換＆格納
            if (divStrArray[0] == "ParamInt")
                this.ParamInt = Convert.ToInt32(divStrArray[1]);  //文字列をInt変換

            //### Float値を変換＆格納
            if (divStrArray[0] == "ParamFloat")
                this.ParamFloat = Convert.ToSingle(divStrArray[1]);  //文字列をFloat変換

            //### String格納
            if (divStrArray[0] == "ParamString")
                this.ParamString = divStrArray[1];
            */


        }

        fsReader.Close();

        return true;
    }




    //################ 設定ファイルセーブメソッド ##################
    public bool UtilSaveSettingFile(string setSaveFilePath)
    {
        StreamWriter fsWriter = new StreamWriter(setSaveFilePath, false, System.Text.Encoding.UTF8);

        fsWriter.WriteLine("UnityAccessURL=" + this.UnityAccessURL);
        fsWriter.WriteLine("UserPostingPageURL=" + this.UserPostingPageURL);


        fsWriter.Close();

        return true;
    }


     //################ パラメータチェックメソッド ##################
    public void    DebugPrint()
    {
        Debug.Log("UnityAccessURL :: " + this.UnityAccessURL);
        Debug.Log("UserPostingPageURL :: " + this.UserPostingPageURL);
    }


}


