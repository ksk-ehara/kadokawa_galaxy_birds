﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;        
using ZXing.QrCode; 

public class MakeQRAccessCodeURL : MonoBehaviour
{
    private string _baseURL;

    public  GameObject qrcodePlane;
    private Texture2D tex2D;//エンコードして出来たQRコードのTxture2Dが入る
    private int ImageWidth = 256;
    private int ImageHeight = 256;
    
    void Start()
    {
        
    }


    //########## ベースのURLセット
    public  void UtilSetBaseURL( string  setBaseURL)
    {
        this._baseURL = setBaseURL;
    }


    //########## QRコードプレーンのVisible設定
    //201030-add
    public void UtilSetVisibleQRCodePlane(bool setStatus)
    {
        this.qrcodePlane.GetComponent<MeshRenderer>().enabled = setStatus;
    }
    //201030-end

    //########## アクセスコードセット
    public void UtilMakeQRAccessCodeURL(string  setAccessCode)
    {
        string  qrcodeURL = this._baseURL + "?code=" + setAccessCode;        

        this.qrcodePlane.GetComponent<MeshRenderer>().enabled = true;
        
        //空のテクスチャを作成
        tex2D = new Texture2D(ImageWidth, ImageHeight);

        //エンコード処理
        var color32 = Encode( qrcodeURL, ImageWidth, ImageHeight);

        tex2D.SetPixels32(color32);
        tex2D.Apply();
        qrcodePlane.GetComponent<Renderer>().material.SetTexture("_MainTex", tex2D);
    }




    //エンコード処理（ここはサンプル通り）
    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var qrcode = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.L, //重要!! QRコードの品質。googleと合わせている
                Height = height,
                Width = width,
                Margin = 1
                
            }
        };
        return qrcode.Write(textForEncoding);
    }



}
