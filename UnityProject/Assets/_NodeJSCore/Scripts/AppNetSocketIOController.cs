﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;


public class AppNetSocketIOController : MonoBehaviour
{

    private Socket _socketio;
    private string _AccessURL = "";
    private bool _isServerConnectStatus = false;
    public ManageARUser _refARUserManager;
    public MakeQRAccessCodeURL _refQRAccessCodeURL;

    //add-201106
    private bool                HandShakeEnable = true;
    private float               fHandShakeSecTime = 60.0f;
    private float               fHandShakePassSecTime = 0.0f;    
    private ulong               uHandShakeCounter = 0;

    //end-201106

    private int _updateID = -1;
    private int _updateID_Connect_Success = 1;
    private int _updateID_Connect_Error = 2;
    private int _updateID_Disconnect = 3;
    private int _updateID_StoU_login_user = 4;
    private int _updateID_StoU_disconnect_user = 5;
    private int _updateID_RequestCurrentUserData = 6;
    private int _updateID_StoU_accesscode_recieve = 7;
    private int _updateID_StoU_usercomment_recieve = 8;
    private int _updateID_StoU_handshakehello_recieve  = 9;	

    private JObject _recJObj;


    private Action _cb_Success_ConnectServer;
    private Action _cb_Error_ConnectServer;
    private Action _cb_Disconnect_Server;


    // Start is called before the first frame update
    void Start()
    {

    }


    void Update()
    {

        //##### 接続成功時
        if (this._updateID == this._updateID_Connect_Success)
        {

            if (this._cb_Success_ConnectServer != null)
                this._cb_Success_ConnectServer();

            this._updateID = -1;
        }

        //##### 接続失敗時
        if (this._updateID == this._updateID_Connect_Error)
        {

            if (this._cb_Error_ConnectServer != null)
                this._cb_Error_ConnectServer();

            this._updateID = -1;
        }

        //##### 切断時
        if (this._updateID == this._updateID_Disconnect)
        {

            if (this._cb_Disconnect_Server != null)
                this._cb_Disconnect_Server();

            this._updateID = -1;
        }


        //########　ユーザーがログイン時に呼び出される。
        if (this._updateID == this._updateID_StoU_login_user)
        {

            string getUserID = this._recJObj["UserID"].ToString();
            string getNickName = this._recJObj["NickName"].ToString();
            string getSocketID = this._recJObj["SocketID"].ToString();

            //###### ユーザーデータの追加
            ARUserData userData = new ARUserData();
            userData.userID = getUserID;
            userData.NickName = getNickName;
            userData.SocketID = getSocketID;
            this._refARUserManager.UtilAddUser(getSocketID, userData);

            this._updateID = -1;
        }

        //########　ユーザー切断時に呼び出される
        if (this._updateID == this._updateID_StoU_disconnect_user)
        {

            string strUserID = this._recJObj["UserID"].ToString();
            string strNickName = this._recJObj["NickName"].ToString();
            string getSocketID = this._recJObj["SocketID"].ToString();

            //##### ユーザーの削除
            this._refARUserManager.UtilDeleteUser(getSocketID);

            //   Debug.Log("ユーザー切断 ID = " + strUserID + "  :: NickName = " + strNickName);
            this._updateID = -1;
        }


        //######### サーバー側に最新のユーザーデータを要求するメソッド
        if (this._updateID == this._updateID_RequestCurrentUserData)
        {

            string getUserIDs = this._recJObj["UserIDs"].ToString();
            string getNickNames = this._recJObj["NickNames"].ToString();
            string getSocketIDs = this._recJObj["SocketIDs"].ToString();

            //Debug.Log(getUserIDs); Debug.Log(getNickNames); Debug.Log(getSocketIDs);

            string[] divUserIDs = getUserIDs.Split(new char[] { ',' });
            string[] divNickNames = getNickNames.Split(new char[] { ',' });
            string[] divSocketIDs = getSocketIDs.Split(new char[] { ',' });

            for (int i = 0; i < divUserIDs.Length; i++)
            {
                //###### ユーザーデータの追加
                ARUserData userData = new ARUserData();
                userData.userID = divUserIDs[i];
                userData.NickName = divNickNames[i];
                userData.SocketID = divSocketIDs[i];

                if (userData.userID.Length == 0 || userData.SocketID.Length == 0)
                    continue;

                this._refARUserManager.UtilAddUser(userData.SocketID, userData);
            }

            this._updateID = -1;
        }

        //######## サーバーからアクセスコードを受信した時
        if (this._updateID == this._updateID_StoU_accesscode_recieve)
        {

            string strAccessCode = this._recJObj["AccessCode"].ToString();
            this._refQRAccessCodeURL.UtilMakeQRAccessCodeURL(strAccessCode);

            this._updateID = -1;
        }

        //######## サーバー（ユーザ）からコメントを受け取った時
        if (this._updateID == this._updateID_StoU_usercomment_recieve)
        {

            string getUserID = this._recJObj["UserID"].ToString();
            string getNickName = this._recJObj["NickName"].ToString();
            string getSocketID = this._recJObj["SocketID"].ToString();
            string getMessage = this._recJObj["Message"].ToString();
            string getSelectNo = this._recJObj["SelectNo"].ToString();

            ARUserData editUserData = this._refARUserManager.UtilGetUserDataFromSID(getSocketID);
            editUserData.Message = getMessage;
            editUserData.SelectNo = getSelectNo;
            this._refARUserManager.UtiEditUser(getSocketID, editUserData);
            this._updateID = -1;

            var jsonObject = new JObject();
            jsonObject.Add("NickName", getNickName);
            jsonObject.Add("Message", getMessage);

            this._socketio.Emit("UtoS_ok_recieve_message", jsonObject);
        }


        //######## サーバーからUnityへのHandShake処理
        if( this._updateID == this._updateID_StoU_handshakehello_recieve ){
            string strHandShakeNum = this._recJObj["HandShakeCount"].ToString();            
            //Debug.Log("HandShake :: Num = " + strHandShakeNum);
            this._updateID = -1;            
        }


        //############ UnityからサーバーへのHandShake処理 ###############
        if( this.HandShakeEnable == true ){
            this.fHandShakePassSecTime += Time.deltaTime;
            if( this.fHandShakePassSecTime > this.fHandShakeSecTime){
              
              var jsonObject = new JObject();        
              jsonObject.Add("HandShakeCount", this.uHandShakeCounter.ToString());      
              this._socketio.Emit("UtoS_HandShake_Hello", jsonObject);            
              this.fHandShakePassSecTime = 0.0f;
              this.uHandShakeCounter ++;
            }
        }
        //########################################

    }

    public void UtilSetup(ManageARUser setARUserManager, MakeQRAccessCodeURL setQRAccessCodeURL)
    {
        this._refARUserManager = setARUserManager;
        this._refQRAccessCodeURL = setQRAccessCodeURL;
    }



    //############## サーバー接続メソッド ###################
    public void UtilConnectNodeJSServer(string setURL, Action cbSuccess, Action cbError = null)
    {

        if (this._isServerConnectStatus == true)
            return;


        this._socketio = IO.Socket(setURL);

        //##### 接続成功時
        this._socketio.On(Socket.EVENT_CONNECT, () =>
        {
            this._isServerConnectStatus = true;

            this._cb_Success_ConnectServer = cbSuccess;
            this._updateID = this._updateID_Connect_Success;
        });

        //##### 接続失敗時
        this._socketio.On(Socket.EVENT_CONNECT_ERROR, () =>
        {
            this._cb_Error_ConnectServer = cbError;
            this._updateID = this._updateID_Connect_Error;
        });
    }


    //############## サーバー切断メソッド ###################
    public void UtilDisconnectNodeJSServer(Action cbClose = null)
    {

        if (this._isServerConnectStatus == false)
            return;

        this._socketio.Disconnect();

        this._cb_Disconnect_Server = cbClose;
        this._updateID = this._updateID_Disconnect;
    }



    //############# サーバーログインメソッド #############

    public void UtilLoginServer()
    {

        //サーバーへの接続
        this._socketio.Emit("UtoS_request_connect");
        this._socketio.On("StoU_response_connect_OK", () => {
            // 今現在サーバーにアクセスしているユーザー情報を取得
            this._RequestCurrentUserData();
        });


        //ユーザーがログイン時
        this._socketio.On("StoU_login_user", (obj) => {
            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_StoU_login_user;
        });

        //ユーザー切断時
        this._socketio.On("StoU_disconnect_user", (obj) => {
            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_StoU_disconnect_user;
        });


        //サーバーへの接続
        this._socketio.Emit("UtoS_request_connect");
        this._socketio.On("StoU_response_connect_OK", (jo) => {

            // 今現在サーバーにアクセスしているユーザー情報を取得
            this._RequestCurrentUserData();
        });


        //サーバーからアクセスコードを受け取った時
        this._socketio.On("StoU_send_accesscode", (obj) => {
            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_StoU_accesscode_recieve;
        });


        //サーバー（ユーザ）からコメントを受け取った時
        this._socketio.On("StoU_send_usercomment", (obj) => {
            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_StoU_usercomment_recieve;
        });


         //HandShake処理
        this._socketio.On("StoU_HandShake_Hello", (obj)=>{
            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_StoU_handshakehello_recieve;
        });
        
    }



    //######### サーバー側に最新のユーザーデータを要求するメソッド ##########
    private void _RequestCurrentUserData()
    {
        this._refARUserManager.UtilAllClearUser();      //すべてのユーザーデータを削除

        this._socketio.Emit("UtoS_request_current_userdata");
        this._socketio.On("StoU_response_current_userdata", (object obj) => {

            this._recJObj = JObject.FromObject(obj);
            this._updateID = this._updateID_RequestCurrentUserData;
        });
    }


    void OnApplicationQuit()
    {

        this.UtilDisconnectNodeJSServer();
    }
}
