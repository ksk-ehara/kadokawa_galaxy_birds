﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;


public  class ARUserData
{
    public string userID;
    public string NickName;
    public string SocketID;

    public string SelectNo;

    public string Message;

    public ARUserData()
    {

    }

    public ARUserData UtilCopy()
    {
        ARUserData copyData = new ARUserData();

        copyData.userID = this.userID;
        copyData.NickName = this.NickName;
        copyData.SocketID = this.SocketID;
        copyData.SelectNo = this.SelectNo;        
        copyData.Message = this.Message;

        return copyData;
    }


    public  void    UtilPrint()
    {
        Debug.Log("ID = " + this.userID + "  NickName = " + this.NickName + " SocketID = " + this.SocketID + " SelectNo = " + this.SelectNo + " Message = " + this.Message);
    }



}

