﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;


public class ManageARUser
{
    //###### 今現在アクセスしているユーザ情報を格納する
    //###### ※ソケットのセッションIDをキー名としたハッシュ配列。
    public Dictionary<string, ARUserData> CurrentAccessUserInfo;

    //######　ユーザーIDからセッションIDを取得するためのハッシュ配列
    //######　キー名::ユーザーID, 値がセッションID
    public Dictionary<string, string> ReferSocketIDFromUserID;


    //###### カスタムイベント
    public event System.EventHandler cbAddUser;
    public event System.EventHandler cbDeleteUser;
    public event System.EventHandler cbEditUserData;
    public event System.EventHandler cbAllClearUserData;



    //#####
    //##### コンストラクタ
    //#####
    public ManageARUser()
    {
        this.CurrentAccessUserInfo = new Dictionary<string, ARUserData>();
        this.ReferSocketIDFromUserID = new Dictionary<string, string>();
    }


 
    //############### 今現在のユーザー数を返す ##########
    public  int    ARUserNum
    {
        get { return this.CurrentAccessUserInfo.Count; }
    }



    //############## 指定されたソケットIDのユーザーデータが存在するかを返す ############
    public bool    UtilExistsCheckUserFromSID(string setSocketID)
    {
        if (this.CurrentAccessUserInfo.ContainsKey(setSocketID) == false)
            return false;

        return true;
    }

    //############## ソケットIDからユーザーデータを返すメソッド ############
    public  ARUserData UtilGetUserDataFromSID(string setSocketID)
    {
        if (this.CurrentAccessUserInfo.ContainsKey(setSocketID) == false)
            return null;

        ARUserData userData = this.CurrentAccessUserInfo[setSocketID];

        return userData;
    }


    //############## ユーザーIDからユーザーデータを返すメソッド ############
    public ARUserData UtilGetUserDataFromUID(string setUserID)
    {
        if (this.ReferSocketIDFromUserID.ContainsKey(setUserID) == false)
            return null;

        string socketID = this.ReferSocketIDFromUserID[setUserID];
        ARUserData userData = this.CurrentAccessUserInfo[socketID];

        return userData;
    }


    //############# ユーザー追加メソッド　###############
    public bool UtiEditUser(string setSocketID, ARUserData editUserData)
    {
        this.CurrentAccessUserInfo[setSocketID] = editUserData;
        this.ReferSocketIDFromUserID[editUserData.userID] = setSocketID;

        //カスタムイベント発生
        if (this.cbEditUserData != null)
        {
            object obj = new object();
            EventArgs evt = new EventArgs();
            obj = editUserData;
            this.cbEditUserData(obj, evt);
        }

        return true;
    }


    //############# ユーザー追加メソッド　###############
    public bool UtilAddUser(string setSocketID, ARUserData setUserData)
    {
        this.CurrentAccessUserInfo[setSocketID] = setUserData;
        this.ReferSocketIDFromUserID[setUserData.userID] = setSocketID;

        //カスタムイベント発生
        if (this.cbAddUser != null)
        {
            object obj = new object();
            EventArgs evt = new EventArgs();
            obj = setUserData;
            this.cbAddUser(obj, evt);
        }


        return true;
    }


    //############# ユーザー削除メソッド　###############
    public void UtilDeleteUser(string setSocketID) {

        if (this.CurrentAccessUserInfo.ContainsKey(setSocketID) == false)
            return;

        ARUserData deleteUserData = this.CurrentAccessUserInfo[setSocketID].UtilCopy();
        string deleteUserID = this.CurrentAccessUserInfo[setSocketID].userID;

        this.ReferSocketIDFromUserID.Remove(deleteUserID);
        this.CurrentAccessUserInfo.Remove(setSocketID);

        //カスタムイベント発生
        if (this.cbDeleteUser != null)
        {
            object obj = new object();
            EventArgs evt = new EventArgs();
            obj = deleteUserData;
            this.cbDeleteUser(obj, evt);
        }
    }


    //############# 全ユーザー削除メソッド　###############
    public  void    UtilAllClearUser()
    {
        this.CurrentAccessUserInfo.Clear();
        this.ReferSocketIDFromUserID.Clear();

        //カスタムイベント発生
        if (this.cbAllClearUserData != null)
        {
            object obj = new object();
            EventArgs evt = new EventArgs();
            this.cbAllClearUserData(obj, evt);
        }
    }



    //############# デバッグ出力メソッド #############
    public void UtilPrint()
    {
        Debug.Log("===========================");
        Debug.Log("UserNum = " + this.CurrentAccessUserInfo.Count);
        Debug.Log("-------------------");
        foreach( string key in this.CurrentAccessUserInfo.Keys)
        {
            ARUserData userData = this.CurrentAccessUserInfo[key];
            userData.UtilPrint();
        }
        Debug.Log("===========================");

    }
}




