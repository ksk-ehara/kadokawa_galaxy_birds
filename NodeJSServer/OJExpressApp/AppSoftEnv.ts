
export default class AppSoftEnv {

    public param: any =
    {
            "hostname": "127.0.0.1",
//          "hostname": "0.0.0.0",

            "Unity_Port": 8080,                 //Unity接続ポート
            "Client_Port": 10443,               //クライアント接続ポート

            "AccessCodeNeeds": true,            //アクセスコード有効・無効
            "ChangeAccessCodeSecTime": 3600,      //アクセスコードを変更する時間（秒数）
            "DebugPrintAccessCode":false,        //アクセスコードのデバッグ出力の有効・無効
    };


}

