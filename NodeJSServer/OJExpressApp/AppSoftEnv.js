"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppSoftEnv {
    constructor() {
        this.param = {
            "hostname": "127.0.0.1",
            //          "hostname": "0.0.0.0",
            "Unity_Port": 8080,
            "Client_Port": 10443,
            "AccessCodeNeeds": true,
            "ChangeAccessCodeSecTime": 3600,
            "DebugPrintAccessCode": false,
        };
    }
}
exports.default = AppSoftEnv;
//# sourceMappingURL=AppSoftEnv.js.map