"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log4js = require("log4js");
const express = require("express");
const helmet = require("helmet");
const path = require("path");
const index_1 = require("./routes/index");
//import routesAR from './routes/ar';
const pm_1 = require("./routes/pm");
const UtilDate_1 = require("./omniutil/UtilDate");
const UtilUserData_1 = require("./omniutil/UtilUserData");
const UtilManageUser_1 = require("./omniutil/UtilManageUser");
const UtilAccessCode_1 = require("./omniutil/UtilAccessCode");
const AppSoftEnv_1 = require("./AppSoftEnv");
var appSoftEnv = new AppSoftEnv_1.default();
var hostname = appSoftEnv.param["hostname"];
var userHttpPort = appSoftEnv.param["Client_Port"]; //https://localhost:10443/ �ŃA�N�Z�X
var unityHttpPort = appSoftEnv.param["Unity_Port"]; //http://localhost:8888/ �ŃA�N�Z�X
//##### �ʐM�֘A
var haertBeatHourTime = 24;
var Unity_HandShakeEnable = true;
var Unity_fHandShakeSecTime = 60.0;
var Unity_uHandShakeCounter = 0;
var Client_HandShakeEnable = true;
var Client_fHandShakeSecTime = 5.0;
var Client_uHandShakeCounter = 0;
//##### AR���[�U�[�i�N���C�A���g�j
var _arUserManager = new UtilManageUser_1.default(); //���[�U�[�Ǘ�
//#####  UnitySocket
//�\�P�b�g�̃Z�b�V����ID���L�[���Ƃ����n�b�V���z��B
var _AllUnitySockets = {};
//#####  �Ǘ��ҁiadmin�j
var _adminSocketID = null;
var _adminSocket = null;
//#####  �Ǘ��҃��j�^
var _adminMonitorSocketID = null;
var _adminMonitorSocket = null;
//#####  ���[�U�[����f�[�^�𑗐M����ۂɃR�[�h���K�v�ɂȂ邩�ǂ���
var _SendDataAccessCodeNeeds = appSoftEnv.param["AccessCodeNeeds"];
var _ChangeAccessCodeSecTime = appSoftEnv.param["ChangeAccessCodeSecTime"]; //�A�N�Z�X�R�[�h��ύX���鎞��(�b�w��)
var _DebugPrintAccessCode = appSoftEnv.param["DebugPrintAccessCode"];
var _utilAccessCode = new UtilAccessCode_1.default();
_utilAccessCode.UtilDebugPrintCode();
//#####
//############### �ȉ� Express���� ##############
const app = express();
//############## �ȉ��Z�L�����e�B�֘A ##############
//app.use(helmet());
app.use(helmet.xssFilter());
app.use(helmet.hidePoweredBy());
/*
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
*/
/*
app.set('trust proxy', 1) // trust first proxy
app.use(session({
    secret: 's3Cur3',
    name: 'sessionId'
}))
*/
app.disable('x-powered-by');
//###
//############## �ȉ����O�ݒ� ##############
log4js.configure('./log/log4js.config.json');
const systemLogger = log4js.getLogger('system');
const httpLogger = log4js.getLogger('http');
const logger = log4js.getLogger('access');
// Express �� �W�����O�o�͂� log4js �ɏ�������
app.use(log4js.connectLogger(systemLogger, { level: 'auto' }));
//app.use(log4js.connectLogger(httpLogger, { level: 'auto' }));
//############### �ȉ��uUser HTTP�v�T�[�o�[�N�� ##############
var userHttp = require('http');
var userHttpServer = userHttp.createServer(app);
userHttpServer.listen(userHttpPort, hostname, () => {
    logger.info('=== Client Start HttpServer :: Connect IP & Port :: ' + hostname + ' listening on port :: ' + userHttpServer.address().port);
});
//############### �ȉ��uUnity HTTP�v�T�[�o�[�N�� ##############
var unityHttp = require('http');
var unityHttpServer = unityHttp.createServer(app);
unityHttpServer.listen(unityHttpPort, hostname, () => {
    logger.info('=== Unity Start HttpServer ::  IP & Port :: ' + hostname + ' listening on port :: ' + unityHttpServer.address().port);
});
//##########
//############### �ȉ��t�H���_���A���[�e�B���O�ݒ� ############
//#### �d�v!! :: �ȉ�public�t�H���_����AR�̃R���e���c������
app.use(express.static(path.join(__dirname, 'public')));
//#### ���[�e�B���O
app.use('/', index_1.default);
app.use('/pm', pm_1.default);
//#### Jade(Pug)�̐ݒ� �����ۂ͎g��Ȃ�
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
//#######
//#######
//#######
//################################################################################
//###################### �����[�U�[�p :: �uHTTP�v��Socket.IO���쐬  ##############
//################################################################################
const userHttpSocketIO = require('socket.io')(userHttpServer);
userHttpSocketIO.on('connection', (clientSocket) => {
    logger.info("Client :: Socket.IO :: Connection !! " + clientSocket.id);
    //################# AR���[�U�[����̃R���e���c�Q�� #################
    clientSocket.on("CtoS_request_login", (data) => {
        //### �N���C�A���g�Ǘ��p�n�b�V���z��ɒl���i�[�B
        var newUser = new UtilUserData_1.default();
        newUser.userID = _arUserManager.UtilIssueUserID(); //���[�UID
        newUser.NickName = data['nickname']; //�j�b�N�l�[��       
        newUser.AccessDate = UtilDate_1.default.UtilGetCurrentTime(); //�Q������
        newUser.SocketID = clientSocket.id; //SocketID
        //#### �Ǘ��N���X�ɃZ�b�g
        _arUserManager.UtilAddUser(clientSocket.id, newUser);
        var sendData = newUser.UtilMakeUserHashData();
        logger.info("CtoS_login :: " + clientSocket.id + "  NickName = " + data['nickname']);
        //### Server => Unity :: ���[�U�[�����O�C����������Unity�ɒʒm
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            unitySocket.emit("StoU_login_user", sendData);
        }
        //### Server => AdminMonitor ::  ���[�U�[��񂪍X�V��������`����
        if (_adminMonitorSocket != null) {
            _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo", sendData);
        }
    });
    //############# �N���C�A���g���烁�b�Z�[�W���͂�
    clientSocket.on("CtoS_message_send", (strJson) => {
        var data = JSON.parse(strJson);
        logger.info("CtoS_message_send :: " + clientSocket.id + "  NickName = " + data['nickname'] + "  AccessCode = " + data['accesscode'] + " SelectNo = " + data['selectno'] + " Message = " + data['message']);
        //####### �A�N�Z�X�R�[�h�̃`�F�b�N
        var status = true;
        if (_SendDataAccessCodeNeeds == true) {
            //### �A�N�Z�X�R�[�h�����݂��邩���`�F�b�N
            status = _utilAccessCode.UtilExistsAccessCode(data['accesscode']);
            //### �������݂��Ȃ���΃N���C�A���g�փG���[���M
            if (status == false) {
                //                var sendData: any = { "message": "�G���[ :: �y�[�W�̗L���������؂�܂���" };
                var sendData = { "message": "Error Server Connect TimeOut" };
                clientSocket.emit("AdminoToC_Message_from_Admin", sendData);
                return;
            }
        }
        //####### Unity�ւ̑��M
        var userData = _arUserManager.UtilGetUserDataFromSID(clientSocket.id);
        if (status == true && userData != null) {
            userData.SelectNo = data['selectno'];
            userData.Message = data['message'];
            var sendUserData = userData.UtilMakeUserHashData();
            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                unitySocket.emit("StoU_send_usercomment", sendUserData);
                logger.info("StoU_send_usercomment :: " + clientSocket.id + "  NickName = " + data['nickname'] + "  AccessCode = " + data['accesscode'] + " SelectNo = " + data['selectno'] + " Message = " + data['message']);
            }
        }
    });
    //## �N���C�A���g�̃��O�A�E�g ���������Z�b�V�����i�\�P�b�g�j�͒����Ă���B
    clientSocket.on("CtoS_request_logout", () => {
        var socketID = clientSocket.id;
        var logoutUserData = _arUserManager.UtilGetUserDataFromSID(socketID);
        if (logoutUserData != null) {
            logger.info("CtoS_request_logout :: " + logoutUserData.NickName);
            var sendData = logoutUserData.UtilMakeUserHashData();
            _arUserManager.UtilDeleteUser(logoutUserData.SocketID); //�Ǘ��f�[�^����͍폜
            //### Server => Unity :: ���[�U�[�����O�A�E�g��������ʒm
            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                unitySocket.emit("StoU_disconnect_user", sendData);
            }
            //### Server => AdminMonitor ::  ���[�U�[��񂪍X�V��������`����
            if (_adminMonitorSocket != null) {
                _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo", sendData);
            }
            //#### �o��
            //_arUserManager.UtilPrint();
        }
    });
    /*
    //################## �Ǘ��҂���̎Q�� ########################
    clientSocket.on("AdminToS_request_connect", (data) => {

        logger.info("Admin Connect :: " + clientSocket.id);

        _adminSocket = clientSocket;
        _adminSocketID = clientSocket.id;

        //####### �Ǘ����ɐڑ�������������`����
        var sendData: any = { 'CurrentAccessUserInfo': _arUserManager.CurrentAccessUserInfo };
        _adminSocket.emit("StoAdmin_connect_OK", sendData);


        //######### �Ǘ��ґ��ł��ׂĂ�AR���[�U�[�Ƀ��b�Z�[�W�𑗂�
        _adminSocket.on("AdminToS_sendmessage_allarusers", (admindata) => {

            var sendMessage: string = admindata["message"];
            //            logger.info("MessageSend!!! :: " + sendMessage);

            //##### AR���[�U�̂��ׂẴ\�P�b�gID���擾
            var socketIDArray: any = _arUserManager.UtilGetAllSocketIDs();
            for (var i: number = 0; i < socketIDArray.length; i++) {
                httpSocketIO02.sockets.to(socketIDArray[i]).emit('AdminoToC_Message_from_Admin', admindata);
            }

        });

    });


    //#################  �Ǘ��҃��j�^����̎Q�� #################
    clientSocket.on("AdminMonitorToS_request_connect", (data) => {

        logger.info("AdminMonitor Connect :: " + clientSocket.id);

        _adminMonitorSocket = clientSocket;
        _adminMonitorSocketID = clientSocket.id;

        //####### ���j�^���ɐڑ�������������`����
        var sendData: any = { 'CurrentAccessUserInfo': _arUserManager.CurrentAccessUserInfo };
        _adminMonitorSocket.emit("StoAdminMonitor_connect_OK", sendData);


        //######### ���j�^���ŉ����ꂽ�{�^���iAR���[�U�[�j�Ƀ��b�Z�[�W�𑗂�
        _adminMonitorSocket.on("AdminMonitorToC_send_client_messsage_from_admin", (admindata) => {

            var sendSocketID: string = admindata["SocketID"];
            //logger.info("Send :: " + sendSocketID);
            //�d�v!! �w�肵���Z�b�V����ID�ɑ΂��ăC�x���g�𔭐�������
            httpSocketIO02.sockets.to(sendSocketID).emit('AdminMonitorToC_Hello_from_Admin');

        });
    });
    */
    //#### �ڑ���Ńu���E�U��������Ɛڑ������Ƃ��ČĂяo�����B
    clientSocket.on("disconnect", (data) => {
        //################# �Ǘ��҂̐ؒf #################
        if (_adminSocketID == clientSocket.id) {
            _adminSocketID = null;
            _adminSocket = null;
            logger.info("Admin Disconnect");
        }
        //################# �Ǘ��ҁiGUI�j�̐ؒf #################
        else if (_adminMonitorSocketID == clientSocket.id) {
            _adminMonitorSocketID = null;
            _adminMonitorSocket = null;
            logger.info("AdminMonitor Disconnect");
        }
        //################# AR���[�U�[�̐ؒf #################
        else {
            //#### ���[�U�[�Ǘ��N���X����폜
            var deleteUserInfo = _arUserManager.UtilDeleteUser(clientSocket.id);
            //### Server => Unity :: ���[�U�[�����O�A�E�g��������ʒm
            if (deleteUserInfo != null) {
                for (var socketID in _AllUnitySockets) {
                    var unitySocket = _AllUnitySockets[socketID];
                    unitySocket.emit("StoU_disconnect_user", deleteUserInfo);
                }
            }
            //### Server => AdminMonitor :: ���[�U�[��񂪍X�V��������`����
            if (_adminMonitorSocket != null) {
                _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo");
            }
            logger.info("Client :: Socket.IO :: Disconnect !! " + clientSocket.id);
            //#### �o��
            //_arUserManager.UtilPrint();
        }
    });
});
//################################################################################
//############### ��Unity�p ::�uHTTP�v��Socket.IO�쐬   ###########################
//################################################################################
const unityHttpSocketIO = require('socket.io')(unityHttpServer);
unityHttpSocketIO.set('heartbeat timeout', haertBeatHourTime * 3600 * 1000);
unityHttpSocketIO.set('heartbeat interval', haertBeatHourTime * 3600 * 1000);
unityHttpSocketIO.on('connection', (clientSocket) => {
    //### Unity =>�@Server :: �ڑ�������ʒm
    clientSocket.on("UtoS_request_connect", (data) => {
        logger.info("Unity3D :: Socket.IO :: Connection !! " + clientSocket.id);
        var unitySocket = clientSocket;
        var unitySocketID = clientSocket.id;
        _AllUnitySockets[unitySocketID] = unitySocket;
        //### Server => Unity :: �ڑ�������ʒm
        unitySocket.emit("StoU_response_connect_OK");
        //### Server => Unity ::�@�A�N�Z�X�R�[�h���L���ȏꍇ�͍ŐV�̃A�N�Z�X�R�[�h�𑗐M����
        if (unitySocket != null && _SendDataAccessCodeNeeds == true) {
            setTimeout(() => {
                var accesscode = _utilAccessCode.UtilGetNewestAccessCode(); //�����ݍŐV�̃A�N�Z�X�R�[�h���擾
                if (accesscode != null) {
                    var sendCode = { "AccessCode": accesscode };
                    //logger.info("SendAccessCode = " + accesscode);
                    unitySocket.emit("StoU_send_accesscode", sendCode);
                }
            }, 2000);
        }
    });
    //#####  Unity =>�@Server :: Unity���Ŗ������b�Z�[�W���󂯎�������ɌĂяo�����B
    clientSocket.on("UtoS_ok_recieve_message", (data) => {
        logger.info("UtoS_ok_recieve_message :: UnityID = " + clientSocket.id + " ::  value = " + data["Message"]);
    });
    //#####  Unity =>�@Server :: HandShake
    clientSocket.on("UtoS_HandShake_Hello", (data) => {
        logger.info("UtoS_HandShake_Hello :: UnityID = " + clientSocket.id + " ::  HandShakeCount = " + data["HandShakeCount"]);
    });
    clientSocket.on("UtoS_request_current_userdata", () => {
        var allUsersData = _arUserManager.UtilMakeAllUsersHashData();
        //        console.log(allUsersData);
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            unitySocket.emit("StoU_response_current_userdata", allUsersData);
        }
    });
    //#### �N���C�A���g���Ńu���E�U��������Ɛڑ������Ƃ��ČĂяo�����B
    clientSocket.on("disconnect", (data) => {
        logger.info("Unity3D :: Socket.IO :: Disconnect !! " + clientSocket.id);
        if (clientSocket.id in _AllUnitySockets == true)
            delete _AllUnitySockets[clientSocket.id];
    });
});
//##########
//########## �A�N�Z�X�R�[�h���� #################
if (_SendDataAccessCodeNeeds == true) {
    _utilAccessCode.UtilExec((code) => {
        //#### �f�o�b�O�\��
        if (_DebugPrintAccessCode == true)
            _utilAccessCode.UtilDebugPrintCode();
        //#### Unity�ɃR�[�h�𑗐M
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            var sendCode = { "AccessCode": code };
            unitySocket.emit("StoU_send_accesscode", sendCode);
        }
    }, _ChangeAccessCodeSecTime * 1000);
}
//##########
//########## �n���h�V�F�C�N���� (Server => Unity)#################
if (Unity_HandShakeEnable == true) {
    //### �J��Ԃ�����
    var idIntervalUnity = setInterval(() => {
        //#### Unity�ɃR�[�h�𑗐M
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            var sendData = { "HandShakeCount": Unity_uHandShakeCounter };
            unitySocket.emit("StoU_HandShake_Hello", sendData);
            logger.info("StoU_HandShake_Hello :: UnityID = " + socketID + " ::  HandShakeCount = " + Unity_uHandShakeCounter);
            Unity_uHandShakeCounter++;
        }
    }, Unity_fHandShakeSecTime * 1000);
}
//##########
//########## �n���h�V�F�C�N���� (Server => �N���C�A���g)#################
if (Client_HandShakeEnable == true) {
    //### �J��Ԃ�����
    var idIntervalClient = setInterval(() => {
        //##### AR���[�U�̂��ׂẴ\�P�b�gID���擾
        var socketIDArray = _arUserManager.UtilGetAllSocketIDs();
        for (var i = 0; i < socketIDArray.length; i++) {
            userHttpSocketIO.sockets.to(socketIDArray[i]).emit('StoC_HandShake_Hello');
            logger.info("StoC_HandShake_Hello :: SocketID = " + socketIDArray[i]);
        }
    }, Client_fHandShakeSecTime * 1000);
}
//##########
//#####  Server => Unity�@ :: HandShake
//###############
//############### �ȉ��G���[����
//###############
// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
//###############################################################
//# sourceMappingURL=app.js.map