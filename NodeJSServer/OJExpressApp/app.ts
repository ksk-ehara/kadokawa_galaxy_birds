import debug = require('debug');
import log4js = require('log4js');
import express = require('express');
import session  = require('express-session');
import helmet = require('helmet');
import path = require('path');

import routesIndex from './routes/index';
//import routesAR from './routes/ar';
import routesPostingMsg from './routes/pm';
import routesAdmin from './routes/admin';
import routesAdminMonitor from './routes/adminmonitor';

import UtilDate from './omniutil/UtilDate';
import UtilUserData from './omniutil/UtilUserData';
import UtilUserManager from './omniutil/UtilManageUser';
import UtilAccessCode from './omniutil/UtilAccessCode';
import UtilAppSoftEnv from './AppSoftEnv';



var appSoftEnv: UtilAppSoftEnv = new UtilAppSoftEnv();

var hostname: string = appSoftEnv.param["hostname"];
var userHttpPort: number = appSoftEnv.param["Client_Port"];  //https://localhost:10443/ でアクセス
var unityHttpPort: number = appSoftEnv.param["Unity_Port"];   //http://localhost:8888/ でアクセス

//##### 通信関連
var haertBeatHourTime: number = 24;

var Unity_HandShakeEnable: boolean = true;
var Unity_fHandShakeSecTime: number = 60.0;
var Unity_uHandShakeCounter: number = 0;

var Client_HandShakeEnable: boolean = true;
var Client_fHandShakeSecTime: number = 5.0;
var Client_uHandShakeCounter: number = 0;




//##### ARユーザー（クライアント）
var _arUserManager: UtilUserManager = new UtilUserManager();   //ユーザー管理

//#####  UnitySocket
//ソケットのセッションIDをキー名としたハッシュ配列。
var _AllUnitySockets = {};

//#####  管理者（admin）
var _adminSocketID: string = null;
var _adminSocket = null;


//#####  管理者モニタ
var _adminMonitorSocketID: string = null;
var _adminMonitorSocket = null;


//#####  ユーザーからデータを送信する際にコードが必要になるかどうか
var _SendDataAccessCodeNeeds: Boolean = appSoftEnv.param["AccessCodeNeeds"]; 
var _ChangeAccessCodeSecTime: number = appSoftEnv.param["ChangeAccessCodeSecTime"];    //アクセスコードを変更する時間(秒指定)
var _DebugPrintAccessCode: Boolean = appSoftEnv.param["DebugPrintAccessCode"];

var _utilAccessCode: UtilAccessCode = new UtilAccessCode();
_utilAccessCode.UtilDebugPrintCode();
//#####



//############### 以下 Express生成 ##############
const app = express();


//############## 以下セキュリティ関連 ##############
//app.use(helmet());
app.use(helmet.xssFilter());
app.use(helmet.hidePoweredBy());

/*
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
*/
/*
app.set('trust proxy', 1) // trust first proxy
app.use(session({
    secret: 's3Cur3',
    name: 'sessionId'
}))
*/

app.disable('x-powered-by');
//###


//############## 以下ログ設定 ##############
log4js.configure('./log/log4js.config.json');

const systemLogger  = log4js.getLogger('system');
const httpLogger    = log4js.getLogger('http');
const logger = log4js.getLogger('access');
// Express の 標準ログ出力を log4js に書き換え
app.use(log4js.connectLogger(systemLogger, { level: 'auto' }));
//app.use(log4js.connectLogger(httpLogger, { level: 'auto' }));


//############### 以下「User HTTP」サーバー起動 ##############
var userHttp = require('http');
var userHttpServer = userHttp.createServer(app);
userHttpServer.listen(userHttpPort, hostname, () => {
    logger.info('=== Client Start HttpServer :: Connect IP & Port :: ' + hostname + ' listening on port :: ' + userHttpServer.address().port);
});



//############### 以下「Unity HTTP」サーバー起動 ##############
var unityHttp = require('http');
var unityHttpServer = unityHttp.createServer(app);
unityHttpServer.listen(unityHttpPort, hostname, () => {
    logger.info('=== Unity Start HttpServer ::  IP & Port :: ' + hostname + ' listening on port :: ' + unityHttpServer.address().port);
});
//##########



//############### 以下フォルダ許可、ルーティング設定 ############

//#### 重要!! :: 以下publicフォルダ内にARのコンテンツを入れる
app.use(express.static(path.join(__dirname, 'public')));

//#### ルーティング
app.use('/', routesIndex);
app.use('/pm', routesPostingMsg);

//#### Jade(Pug)の設定 ※実際は使わない
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


//#######
//#######
//#######


//################################################################################
//###################### ※ユーザー用 :: 「HTTP」でSocket.IOを作成  ##############
//################################################################################

const userHttpSocketIO = require('socket.io')(userHttpServer);
userHttpSocketIO.on('connection', (clientSocket) => {

    logger.info("Client :: Socket.IO :: Connection !! " + clientSocket.id);
   
    //################# ARユーザーからのコンテンツ参加 #################
    clientSocket.on("CtoS_request_login", (data) => {

        //### クライアント管理用ハッシュ配列に値を格納。
        var newUser: UtilUserData = new UtilUserData();
        newUser.userID = _arUserManager.UtilIssueUserID();   //ユーザID
        newUser.NickName = data['nickname'];                 //ニックネーム       
        newUser.AccessDate = UtilDate.UtilGetCurrentTime();  //参加日時
        newUser.SocketID = clientSocket.id;                  //SocketID

        //#### 管理クラスにセット
        _arUserManager.UtilAddUser(clientSocket.id, newUser);

        var sendData = newUser.UtilMakeUserHashData();

        logger.info("CtoS_login :: " + clientSocket.id + "  NickName = " + data['nickname']);

        //### Server => Unity :: ユーザーがログインした事をUnityに通知
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            unitySocket.emit("StoU_login_user", sendData);
        }

        //### Server => AdminMonitor ::  ユーザー情報が更新した事を伝える
        if (_adminMonitorSocket != null) {
            _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo", sendData);
        }
    });


    //############# クライアントからメッセージが届く
    clientSocket.on("CtoS_message_send", (strJson) => {

        var data: any = JSON.parse(strJson);
        logger.info("CtoS_message_send :: " + clientSocket.id + "  NickName = " + data['nickname'] + "  AccessCode = " + data['accesscode'] + " SelectNo = " + data['selectno'] + " Message = " + data['message']);


        //####### アクセスコードのチェック
        var status: Boolean = true;
        if (_SendDataAccessCodeNeeds == true) {

            //### アクセスコードが存在するかをチェック
            status = _utilAccessCode.UtilExistsAccessCode(data['accesscode']);
            //### もし存在しなければクライアントへエラー送信
            if (status == false) {
//                var sendData: any = { "message": "エラー :: ページの有効期限が切れました" };
                var sendData: any = { "message": "Error Server Connect TimeOut" };
                clientSocket.emit("AdminoToC_Message_from_Admin", sendData);
                return;
            }
        }

        //####### Unityへの送信
        var userData: UtilUserData = _arUserManager.UtilGetUserDataFromSID(clientSocket.id);

        if (status == true && userData != null) {

            userData.SelectNo = data['selectno'];
            userData.Message = data['message'];
            var sendUserData: any = userData.UtilMakeUserHashData();

            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                unitySocket.emit("StoU_send_usercomment", sendUserData);
                logger.info("StoU_send_usercomment :: " + clientSocket.id + "  NickName = " + data['nickname'] + "  AccessCode = " + data['accesscode'] + " SelectNo = " + data['selectno'] + " Message = " + data['message']);
            }
        }

    });


    //## クライアントのログアウト ※ただしセッション（ソケット）は張られている。
    clientSocket.on("CtoS_request_logout", () => {

        var socketID: string = clientSocket.id;
        var logoutUserData: UtilUserData = _arUserManager.UtilGetUserDataFromSID(socketID);

        if (logoutUserData != null) {

            logger.info("CtoS_request_logout :: " + logoutUserData.NickName);

            var sendData = logoutUserData.UtilMakeUserHashData();

            _arUserManager.UtilDeleteUser(logoutUserData.SocketID); //管理データからは削除

            //### Server => Unity :: ユーザーがログアウトした事を通知
            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                unitySocket.emit("StoU_disconnect_user", sendData);
            }


            //### Server => AdminMonitor ::  ユーザー情報が更新した事を伝える
            if (_adminMonitorSocket != null) {
                _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo", sendData);
            }

            //#### 出力
            //_arUserManager.UtilPrint();

        }
    });


    /*
    //################## 管理者からの参加 ########################
    clientSocket.on("AdminToS_request_connect", (data) => {

        logger.info("Admin Connect :: " + clientSocket.id);

        _adminSocket = clientSocket;
        _adminSocketID = clientSocket.id;

        //####### 管理側に接続完了した事を伝える
        var sendData: any = { 'CurrentAccessUserInfo': _arUserManager.CurrentAccessUserInfo };
        _adminSocket.emit("StoAdmin_connect_OK", sendData);


        //######### 管理者側ですべてのARユーザーにメッセージを送る
        _adminSocket.on("AdminToS_sendmessage_allarusers", (admindata) => {

            var sendMessage: string = admindata["message"];
            //            logger.info("MessageSend!!! :: " + sendMessage);

            //##### ARユーザのすべてのソケットIDを取得
            var socketIDArray: any = _arUserManager.UtilGetAllSocketIDs();
            for (var i: number = 0; i < socketIDArray.length; i++) {
                httpSocketIO02.sockets.to(socketIDArray[i]).emit('AdminoToC_Message_from_Admin', admindata);
            }

        });

    });


    //#################  管理者モニタからの参加 #################
    clientSocket.on("AdminMonitorToS_request_connect", (data) => {

        logger.info("AdminMonitor Connect :: " + clientSocket.id);

        _adminMonitorSocket = clientSocket;
        _adminMonitorSocketID = clientSocket.id;

        //####### モニタ側に接続完了した事を伝える
        var sendData: any = { 'CurrentAccessUserInfo': _arUserManager.CurrentAccessUserInfo };
        _adminMonitorSocket.emit("StoAdminMonitor_connect_OK", sendData);


        //######### モニタ側で押されたボタン（ARユーザー）にメッセージを送る
        _adminMonitorSocket.on("AdminMonitorToC_send_client_messsage_from_admin", (admindata) => {

            var sendSocketID: string = admindata["SocketID"];
            //logger.info("Send :: " + sendSocketID);
            //重要!! 指定したセッションIDに対してイベントを発生させる
            httpSocketIO02.sockets.to(sendSocketID).emit('AdminMonitorToC_Hello_from_Admin');

        });
    });
    */




    //#### 接続先でブラウザが閉じられると接続解除として呼び出される。
    clientSocket.on("disconnect", (data) => {

        //################# 管理者の切断 #################
        if (_adminSocketID == clientSocket.id) {
            
            _adminSocketID = null;
            _adminSocket = null;

            logger.info("Admin Disconnect");

        }
        //################# 管理者（GUI）の切断 #################
        else if (_adminMonitorSocketID == clientSocket.id) {

            _adminMonitorSocketID = null;
            _adminMonitorSocket = null;

            logger.info("AdminMonitor Disconnect");

        }
        //################# ARユーザーの切断 #################
        else {

            //#### ユーザー管理クラスから削除
            var deleteUserInfo = _arUserManager.UtilDeleteUser(clientSocket.id);

            //### Server => Unity :: ユーザーがログアウトした事を通知
            if (deleteUserInfo != null) {
                for (var socketID in _AllUnitySockets) {
                    var unitySocket = _AllUnitySockets[socketID];
                    unitySocket.emit("StoU_disconnect_user", deleteUserInfo);
                }
            }

            //### Server => AdminMonitor :: ユーザー情報が更新した事を伝える
            if (_adminMonitorSocket != null) {
                _adminMonitorSocket.emit("StoAdminMonitor_update_userinfo");
            }

            logger.info("Client :: Socket.IO :: Disconnect !! " + clientSocket.id);

            //#### 出力
            //_arUserManager.UtilPrint();
        }

    });
     
});



//################################################################################
//############### ※Unity用 ::「HTTP」でSocket.IO作成   ###########################
//################################################################################

const unityHttpSocketIO = require('socket.io')(unityHttpServer);

unityHttpSocketIO.set('heartbeat timeout', haertBeatHourTime * 3600 * 1000);
unityHttpSocketIO.set('heartbeat interval', haertBeatHourTime *  3600 * 1000);
unityHttpSocketIO.on('connection', (clientSocket) => {

    //### Unity =>　Server :: 接続完了を通知
    clientSocket.on("UtoS_request_connect", (data) => {

        logger.info("Unity3D :: Socket.IO :: Connection !! " + clientSocket.id);

        var unitySocket = clientSocket;
        var unitySocketID = clientSocket.id;

        _AllUnitySockets[unitySocketID] = unitySocket;

        //### Server => Unity :: 接続完了を通知
        unitySocket.emit("StoU_response_connect_OK");


        //### Server => Unity ::　アクセスコードが有効な場合は最新のアクセスコードを送信する
        if (unitySocket != null && _SendDataAccessCodeNeeds == true) {

            setTimeout(() => {
                var accesscode: string = _utilAccessCode.UtilGetNewestAccessCode();    //今現在最新のアクセスコードを取得
                if (accesscode != null) {
                    var sendCode: any = { "AccessCode": accesscode };
                    //logger.info("SendAccessCode = " + accesscode);
                    unitySocket.emit("StoU_send_accesscode", sendCode);
                }
            },
            2000);
        }
    });


    //#####  Unity =>　Server :: Unity側で無事メッセージを受け取った時に呼び出される。
    clientSocket.on("UtoS_ok_recieve_message", (data) => {

        logger.info("UtoS_ok_recieve_message :: UnityID = " + clientSocket.id + " ::  value = " + data["Message"]);
    });


    //#####  Unity =>　Server :: HandShake
    clientSocket.on("UtoS_HandShake_Hello", (data) => {

        logger.info("UtoS_HandShake_Hello :: UnityID = " + clientSocket.id + " ::  HandShakeCount = " + data["HandShakeCount"]);
    });



    clientSocket.on("UtoS_request_current_userdata", () => {

        var allUsersData: any = _arUserManager.UtilMakeAllUsersHashData();
//        console.log(allUsersData);
        for (var socketID in _AllUnitySockets) {
            var unitySocket = _AllUnitySockets[socketID];
            unitySocket.emit("StoU_response_current_userdata", allUsersData);
        }
           
    });



    //#### クライアント側でブラウザが閉じられると接続解除として呼び出される。
    clientSocket.on("disconnect", (data) => {

        logger.info("Unity3D :: Socket.IO :: Disconnect !! " + clientSocket.id);

        if (clientSocket.id in _AllUnitySockets == true)
            delete _AllUnitySockets[clientSocket.id];
    });

}); 

//##########


//########## アクセスコード生成 #################

if (_SendDataAccessCodeNeeds == true) {

    _utilAccessCode.UtilExec(
        (code: string) => {

            //#### デバッグ表示
            if (_DebugPrintAccessCode == true)
                _utilAccessCode.UtilDebugPrintCode();

            //#### Unityにコードを送信
            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                var sendCode: any = { "AccessCode": code };
                unitySocket.emit("StoU_send_accesscode", sendCode);
            }

        },
        _ChangeAccessCodeSecTime*1000
    );

}
//##########


//########## ハンドシェイク処理 (Server => Unity)#################

if (Unity_HandShakeEnable == true) {

    //### 繰り返し処理
    var idIntervalUnity= setInterval(

        () => {

            //#### Unityにコードを送信
            for (var socketID in _AllUnitySockets) {
                var unitySocket = _AllUnitySockets[socketID];
                var sendData: any = { "HandShakeCount": Unity_uHandShakeCounter };
                unitySocket.emit("StoU_HandShake_Hello", sendData);
                logger.info("StoU_HandShake_Hello :: UnityID = " + socketID + " ::  HandShakeCount = " + Unity_uHandShakeCounter);
                Unity_uHandShakeCounter++;
            }
        },
        Unity_fHandShakeSecTime * 1000);

}
//##########


//########## ハンドシェイク処理 (Server => クライアント)#################

if (Client_HandShakeEnable == true) {

    //### 繰り返し処理
    var idIntervalClient = setInterval(

        () => {

            //##### ARユーザのすべてのソケットIDを取得
            var socketIDArray: any = _arUserManager.UtilGetAllSocketIDs();
            for (var i: number = 0; i < socketIDArray.length; i++) {
                userHttpSocketIO.sockets.to(socketIDArray[i]).emit('StoC_HandShake_Hello');
                logger.info("StoC_HandShake_Hello :: SocketID = " + socketIDArray[i] );
            }

        },
        Client_fHandShakeSecTime * 1000);

}
//##########




//#####  Server => Unity　 :: HandShake




//###############
//############### 以下エラー処理
//###############


// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err, req, res, next) => { // eslint-disable-line @typescript-eslint/no-unused-vars
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => { // eslint-disable-line @typescript-eslint/no-unused-vars
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//###############################################################




