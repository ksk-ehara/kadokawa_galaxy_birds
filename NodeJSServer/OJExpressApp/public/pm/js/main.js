//- - - - - - - - - -
// 起動時モーダル
//- - - - - - - - - - 
const modal_att = document.getElementById("modal_attention");
document.getElementById("startBtn").addEventListener("click", () => {
    modal_att.classList.add("hidden");
});




//- - - - - - - - - -
// 送信時モーダル
//- - - - - - - - - - 
const modal_snd = document.getElementById("modal_send");
document.getElementById("uiSendBtn").addEventListener("click", () => {
    console.log("click");
    modal_snd.classList.remove("hidden");
    setTimeout(()=>{
        modal_snd.classList.add("hidden");
      }, 8*1000);
});




//- - - - - - - - - -
// swiper
//- - - - - - - - - - 
const swp = new Swiper('.swiper-container', {

    loop: true,

    pagination: {
        el: '.swiper-pagination',
    },

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

})