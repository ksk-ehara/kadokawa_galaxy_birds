"use strict";
//## �N���X�̏�����
//https://qiita.com/gaaamii/items/84ef50277d962fa2c73d
//###
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
class UtilHello {
    constructor() {
        this.valueA = 123;
        this._valueB = 456;
        console.log('UtilHello :: Constructor');
        this.myEvt = new events_1.EventEmitter();
    }
    greeting() {
        console.log('Greeting !! :: A = ' + this.valueA + "  B = " + this._valueB);
    }
    static static_greeting() {
        console.log('Static :: Greeting !!');
    }
    setvalue(setValue) {
        this._valueB = setValue;
        //#### �J�X�^���C�x���g����
        this.myEvt.emit("myEvtSetValue", this._valueB);
    }
}
exports.default = UtilHello;
//######## �g�p��
/*
import UtilHello from './omniutil/_UtilHello';
var hello = new UtilHello();
hello.valueA = 987;
hello.greeting();
UtilHello.static_greeting();

//�C�x���g�o�^�i�J�X�^���j
hello.myEvt.on("myEvtSetValue", (value) => {
    console.log("CallBack = " + value);
});
hello.setvalue(777);    //��L�R�[���o�b�N���Ăяo��

*/
//# sourceMappingURL=_UtilHello.js.map