"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
//import { start } from 'repl';
class UtilManageUser {
    //###############
    //############### �R���X�g���N�^
    //###############
    constructor() {
        this._idUserCounter = 0;
        //###### �����݃A�N�Z�X���Ă��郆�[�U�����i�[����
        //###### ���\�P�b�g�̃Z�b�V����ID���L�[���Ƃ����n�b�V���z��B
        this.CurrentAccessUserInfo = {};
        //######�@���[�U�[ID����Z�b�V����ID���擾���邽�߂̃n�b�V���z��
        //######�@�L�[��::���[�U�[ID, �l���Z�b�V����ID
        this.ReferSocketIDFromUserID = {};
        this.evt = new events_1.EventEmitter();
    }
    //############ �����݂�AR���[�U�[�� #############
    get iUserNum() {
        var keyArray = Object.keys(this.CurrentAccessUserInfo); //�L�[���̎擾
        return keyArray.length;
    }
    //############ �����݂̂��ׂẴ\�P�b�gID��z��ŕԂ� #############
    UtilGetAllSocketIDs() {
        var keyArray = Object.keys(this.CurrentAccessUserInfo); //�L�[���̎擾
        return keyArray;
    }
    //############# SocketID���烆�[�U�[�f�[�^�擾���郁�\�b�h�@###############
    UtilGetUserDataFromSID(setSocketID) {
        if (setSocketID in this.CurrentAccessUserInfo == false)
            return null;
        var userData = this.CurrentAccessUserInfo[setSocketID];
        return userData;
    }
    //############# UserID���烆�[�U�[�f�[�^�擾���郁�\�b�h�@###############
    UtilGetUserDataFromUID(setUserID) {
        if (setUserID in this.ReferSocketIDFromUserID == false)
            return null;
        var socketID = this.ReferSocketIDFromUserID[setUserID];
        var userData = this.CurrentAccessUserInfo[socketID];
        return userData;
    }
    //############# ���[�U�[�ǉ����\�b�h�@###############
    UtilAddUser(setSocketID, setUserData) {
        this.CurrentAccessUserInfo[setSocketID] = setUserData;
        this.ReferSocketIDFromUserID[setUserData.userID] = setUserData.SocketID;
        //#### �J�X�^���C�x���g����
        this.evt.emit("addUser", setUserData);
        return true;
    }
    //############# ���[�U�[�폜���\�b�h�@###############
    UtilDeleteUser(setSocketID) {
        if (setSocketID in this.CurrentAccessUserInfo == false)
            return null;
        var userData = this.CurrentAccessUserInfo[setSocketID];
        console.log("HTTPS :: Socket.IO :: Disconnect !! " + setSocketID + " :: NickName = " + userData.NickName);
        //#### �폜����f�[�^���擾
        var userSimpleData = userData.UtilMakeUserHashData();
        //#### �z�񂩂�폜
        delete this.CurrentAccessUserInfo[setSocketID];
        delete this.ReferSocketIDFromUserID[userData.userID];
        //#### �J�X�^���C�x���g����
        this.evt.emit("deleteUser", userSimpleData);
        return userSimpleData;
    }
    //########## ���[�U�[ID�̔��s���\�b�h ##########
    UtilIssueUserID() {
        var strID = "";
        this._idUserCounter++;
        if (this._idUserCounter >= 10000)
            this._idUserCounter = 0;
        var id = this._idUserCounter;
        if (0 <= id && id < 10)
            strID = "000" + id;
        else if (10 <= id && id < 100)
            strID = "00" + id;
        else if (100 <= id && id < 1000)
            strID = "0" + id;
        return strID;
    }
    //######## ID�̃��Z�b�g���\�b�h ###############
    UtilResetUserIDCounter() {
        this._idUserCounter = 0;
    }
    //#######
    UtilMakeAllUsersHashData() {
        var strUserIDs = "";
        var strNickNames = "";
        var strAccessDates = "";
        var strSocketIDs = "";
        var commaFlag = false;
        for (var key in this.CurrentAccessUserInfo) {
            var userData = this.CurrentAccessUserInfo[key];
            if (commaFlag == true) {
                strUserIDs += ",";
                strNickNames += ",";
                strAccessDates += ",";
                strSocketIDs += ",";
            }
            strUserIDs += userData.userID;
            strNickNames += userData.NickName;
            strAccessDates += userData.AccessDate;
            strSocketIDs += userData.SocketID;
            commaFlag = true;
        }
        var usersData = { "UserIDs": strUserIDs, "NickNames": strNickNames, "AccessDates": strAccessDates, "SocketIDs": strSocketIDs };
        return usersData;
    }
    //##### �f�o�b�O�o�� ###########
    UtilPrint() {
        console.log("====================");
        var keyArray = Object.keys(this.CurrentAccessUserInfo); //�L�[���̎擾
        console.log("UserNum = " + keyArray.length);
        console.log("------------");
        for (var key in this.CurrentAccessUserInfo) {
            var userData = this.CurrentAccessUserInfo[key];
            userData.UtilPrint();
        }
        console.log("====================");
    }
}
exports.default = UtilManageUser;
//# sourceMappingURL=UtilManageUser.js.map