
import { EventEmitter } from "events";
import UtilUserData from './UtilUserData';

//import { start } from 'repl';

export default class UtilManageUser {

    private _idUserCounter: number = 0;


    //###### 今現在アクセスしているユーザ情報を格納する
    //###### ※ソケットのセッションIDをキー名としたハッシュ配列。
    public CurrentAccessUserInfo = {};

    //######　ユーザーIDからセッションIDを取得するためのハッシュ配列
    //######　キー名::ユーザーID, 値がセッションID
    public ReferSocketIDFromUserID = {};


    //#### カスタムイベント
    public evt: EventEmitter;


    //###############
    //############### コンストラクタ
    //###############
    constructor() {

        this.evt = new EventEmitter();

    }


    //############ 今現在のARユーザー数 #############
    get iUserNum(): number
    {
        var keyArray = Object.keys(this.CurrentAccessUserInfo);//キー名の取得
        return keyArray.length;
    }
   

    //############ 今現在のすべてのソケットIDを配列で返す #############
    public UtilGetAllSocketIDs():any {
        var keyArray = Object.keys(this.CurrentAccessUserInfo);//キー名の取得
        return keyArray;

    }


    //############# SocketIDからユーザーデータ取得するメソッド　###############
    public UtilGetUserDataFromSID(setSocketID: string): UtilUserData {

        if (setSocketID in this.CurrentAccessUserInfo == false)
            return null;

        var userData: UtilUserData = this.CurrentAccessUserInfo[setSocketID];

        return userData;
    }


    //############# UserIDからユーザーデータ取得するメソッド　###############
    public UtilGetUserDataFromUID(setUserID: string): UtilUserData{

        if (setUserID in this.ReferSocketIDFromUserID == false)
            return null;

        var socketID: string = this.ReferSocketIDFromUserID[setUserID];
        var userData: UtilUserData = this.CurrentAccessUserInfo[socketID];

        return userData;
    }
    


    //############# ユーザー追加メソッド　###############
    public UtilAddUser(setSocketID: string, setUserData: UtilUserData): boolean {

        this.CurrentAccessUserInfo[setSocketID] = setUserData;
        this.ReferSocketIDFromUserID[setUserData.userID] = setUserData.SocketID;

        //#### カスタムイベント発生
        this.evt.emit("addUser", setUserData);

        return true;
    }


    //############# ユーザー削除メソッド　###############
    public UtilDeleteUser(setSocketID: string): any {

        if (setSocketID in this.CurrentAccessUserInfo == false)
            return null;

        var userData: UtilUserData = this.CurrentAccessUserInfo[setSocketID];

        console.log("HTTPS :: Socket.IO :: Disconnect !! " + setSocketID + " :: NickName = " + userData.NickName);

        //#### 削除するデータを取得
        var userSimpleData: any = userData.UtilMakeUserHashData();

        //#### 配列から削除
        delete this.CurrentAccessUserInfo[setSocketID];
        delete this.ReferSocketIDFromUserID[userData.userID];

        //#### カスタムイベント発生
        this.evt.emit("deleteUser", userSimpleData);

        return userSimpleData;
    }


    //########## ユーザーIDの発行メソッド ##########
    public UtilIssueUserID(): string {

        var strID: string = "";

        this._idUserCounter++;

        if (this._idUserCounter >= 10000)
            this._idUserCounter = 0;

        var id: number = this._idUserCounter;

        if (0 <= id && id < 10)
            strID = "000" + id;
        else if (10 <= id && id < 100)
            strID = "00" + id;
        else if (100 <= id && id < 1000)
            strID = "0" + id;

        return strID;
    }


    //######## IDのリセットメソッド ###############
    public UtilResetUserIDCounter() {
        this._idUserCounter = 0;
    }


    //#######
    public UtilMakeAllUsersHashData(): any {

        var strUserIDs: string = "";
        var strNickNames: string = "";
        var strAccessDates: string = "";
        var strSocketIDs: string = "";

        var commaFlag: boolean = false;

        for (var key in this.CurrentAccessUserInfo) {

            var userData: UtilUserData = this.CurrentAccessUserInfo[key];

            if (commaFlag == true) {
                strUserIDs      += ",";
                strNickNames    += ",";
                strAccessDates  += ",";
                strSocketIDs    += ",";
            }

            strUserIDs      += userData.userID;
            strNickNames    += userData.NickName;
            strAccessDates  += userData.AccessDate;
            strSocketIDs    += userData.SocketID;

            commaFlag = true;
        }

        var usersData = { "UserIDs": strUserIDs, "NickNames": strNickNames, "AccessDates": strAccessDates, "SocketIDs": strSocketIDs };

        return usersData; 
        
    }

    //##### デバッグ出力 ###########
    public UtilPrint() {

        console.log("====================");
        var keyArray = Object.keys(this.CurrentAccessUserInfo);              //キー名の取得
        console.log("UserNum = " + keyArray.length);
        console.log("------------");
        for (var key in this.CurrentAccessUserInfo) {

            var userData: UtilUserData = this.CurrentAccessUserInfo[key];
            userData.UtilPrint();
        }
        console.log("====================");
    } 
}

