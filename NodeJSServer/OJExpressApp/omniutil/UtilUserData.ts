
export default class UtilUserData {

    public userID: string;         //ユーザーID
    public NickName: string;       //ニックネーム
    public AccessDate: string;     //アクセス日時

    //以下実際にソケットも格納したいが、Socketデータ（インスタンス）をクライアント側のHTMLに送る事が出来ないためID（string）だけを格納する
    // public Socket: any     
    public SocketID: string;       //ソケットID        ※Socket.IO

    public SelectNo: string = "";        //選択NO 
    public Message: string="";        //コメント 

    //###############
    //############### コンストラクタ
    //###############
    constructor() {

    }


    //###### ユーザー情報作成メソッド ###########
    public UtilMakeUserHashData(): any {

        var userData = { "UserID": this.userID, "NickName": this.NickName, "AccessDate": this.AccessDate, "SocketID": this.SocketID, "SelectNo": this.SelectNo, "Message": this.Message };
        return userData;
    }


    //##### デバッグ出力 ###########
    public UtilPrint() {

        console.log("ID = " + this.userID + "  NickName = " + this.NickName + " SocketID = " + this.SocketID + " SelectNo = " + this.SelectNo + " Comment = " + this.Message);
    } 



}