//## クラスの書き方
//https://qiita.com/gaaamii/items/84ef50277d962fa2c73d
//###

import { EventEmitter } from "events";


export default class UtilHello {

    public   valueA: number = 123;
    private _valueB: number = 456;

    //#### カスタムイベント
    public   myEvt: EventEmitter;

    constructor() {
        console.log('UtilHello :: Constructor');

        this.myEvt = new EventEmitter();
    }


    public greeting() {
        console.log('Greeting !! :: A = ' + this.valueA + "  B = " + this._valueB );
    }


    public static static_greeting() {

        console.log('Static :: Greeting !!');
    }


    public setvalue(setValue: number) {
        this._valueB = setValue;

        //#### カスタムイベント発生
        this.myEvt.emit("myEvtSetValue", this._valueB );
    }
}



//######## 使用例
/*
import UtilHello from './omniutil/_UtilHello';
var hello = new UtilHello();
hello.valueA = 987;
hello.greeting();
UtilHello.static_greeting();

//イベント登録（カスタム）
hello.myEvt.on("myEvtSetValue", (value) => {
    console.log("CallBack = " + value);
});
hello.setvalue(777);    //上記コールバックを呼び出す

*/

