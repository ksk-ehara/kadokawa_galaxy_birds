

export default class UtilDate {


    //###############
    //############### コンストラクタ
    //###############
    constructor() {

    }


    public static UtilGetCurrentTime() {
        // 現在時刻の取得
        var dt = new Date();

        // 日付を数字として取り出す
        var year: number = dt.getFullYear();
        var month: number = dt.getMonth() + 1;
        var day: number = dt.getDate();
        var hour: number = dt.getHours();
        var min: number = dt.getMinutes();
        var sec: number = dt.getSeconds();
        var miliSec: number = dt.getMilliseconds();

        var sYear: string = year.toString();
        var sMonth: string = month.toString();
        var sDay: string = day.toString();
        var sHour: string = hour.toString();
        var sMin: string = min.toString();
        var sSec: string = sec.toString();
        var sMiliSec: string = miliSec.toString();

        // 値が1桁であれば '0'を追加 

        if (month < 10)
            sMonth = '0' + sMonth;

        if (day < 10)
            sDay = '0' + sDay;

        if (hour < 10)
            sHour = '0' + sHour;

        if (min < 10)
            sMin = '0' + sMin;

        if (sec < 10)
            sSec = '0' + sSec;

        if (miliSec < 10)
            sMiliSec = '000' + sMiliSec;

        if (miliSec < 100)
            sMiliSec = '00' + sMiliSec;

        if (miliSec < 1000)
            sMiliSec = '0' + sMiliSec;
        // 出力
        var Date_now = sYear + sMonth + sDay + sHour + sMin + sSec;


        return Date_now;
    }


    public static UtilGetLogTime() {
        // 現在時刻の取得
        var dt = new Date();

        // 日付を数字として取り出す
        var year: number = dt.getFullYear();
        var month: number = dt.getMonth() + 1;
        var day: number = dt.getDate();
        var hour: number = dt.getHours();
        var min: number = dt.getMinutes();
        var sec: number = dt.getSeconds();
        var miliSec: number = dt.getMilliseconds();

        var sYear: string = year.toString();
        var sMonth: string = month.toString();
        var sDay: string = day.toString();
        var sHour: string = hour.toString();
        var sMin: string = min.toString();
        var sSec: string = sec.toString();
        var sMiliSec: string = miliSec.toString();

        // 値が1桁であれば '0'を追加 

        if (month < 10)
            sMonth = '0' + sMonth;

        if (day < 10)
            sDay = '0' + sDay;

        if (hour < 10)
            sHour = '0' + sHour;

        if (min < 10)
            sMin = '0' + sMin;

        if (sec < 10)
            sSec = '0' + sSec;

        // 出力
        var Date_now = sYear + "/" + sMonth + "/" + sDay + " " + sHour + ":" + sMin + ":" + sSec;

        return Date_now;
    }



}
