import { strictEqual } from "assert";

export default class UtilAccessCode {

    private _idInterval: any = null;

    public codeCharNum: number = 6; //発行（生成）するコードの文字数
    public choiceChar: string = "abcdefghijklmnopqrstuvwxyz0123456789";

    //#### 発行したコードの履歴
    private     _histCodeMaxNum: number = 5;
    private     _histCodeArray: string[] = new Array();
    private     _secretAccessCode: string = "oj5510";

    public       fileBackupEnable: boolean = true;
    private     _BackupFileName = "./_AccessCode.bk";

    //####### コンストラクタ ##############
    constructor() {

        if (this.fileBackupEnable == true)
            this.UtilLoadAccessCode(this._BackupFileName);        
    }



    //############# コードの発行メソッド #############
    public UtilExec(setCallFunc: Function, setInterValSecTime: number) {

        //### 初回
        var code: string = this._MakeAccessCode();
        setCallFunc(code);

        //### 繰り返し処理
        this._idInterval = setInterval(

            () => {

                var code: string = this._MakeAccessCode();               

                // コールバック呼び出し
                setCallFunc(code);
            },
            setInterValSecTime);
    }

    private _MakeAccessCode(): string {

        // 生成する文字列に含める文字セット
        var cl = this.choiceChar.length;
        var code: string = "";
        for (var i = 0; i < this.codeCharNum; i++) {
            code += this.choiceChar[Math.floor(Math.random() * cl)];
        }

        this._histCodeArray.push(code);
        if (this._histCodeArray.length > this._histCodeMaxNum) {
            this._histCodeArray.shift();
        }

        if (this.fileBackupEnable == true)
            this.UtilSaveAccessCode(this._BackupFileName);

        

        return code;
    }


    //############# コードの発行停止 #############
    public UtilStop() {

        if (this._idInterval == null)
            return;

        clearInterval(this._idInterval);   //停止処理
        this._idInterval = null;
    }


    //############# セットされたコードが有効化かどうかをチェックする ############
    public UtilExistsAccessCode(setCheckCode: string): Boolean {

        if (setCheckCode == this._secretAccessCode)
            return true;

        var status: Boolean = false;
        for (var i: number = this._histCodeArray.length - 1; i >= 0; i--) {
            if (setCheckCode == this._histCodeArray[i]) {
                status = true;
                break;
            }
        }
        return status;
    }


    //########## 最新のアクセスコードを返す
    public UtilGetNewestAccessCode(): string {

        if (this._histCodeArray.length == 0)
            return null;

        var code = this._histCodeArray[this._histCodeArray.length - 1];

        return code;
    }



    //　ファイルにアクセスコードを書き出す
    public UtilSaveAccessCode(setSaveFilePath: string) {

        const fs = require('fs');
        var strSave: string = "";
        //for (var i: number = this._histCodeArray.length - 1; i >= 0; i--)
        for (var i: number = 0; i < this._histCodeArray.length; i++)
            strSave += this._histCodeArray[i] + "\n";
        fs.writeFileSync(setSaveFilePath, strSave);
    }


    //　アクセスコードを読み込む
    public UtilLoadAccessCode(setLoadFilePath: string) {

        const fs = require('fs');

        if (fs.existsSync(setLoadFilePath) == true) {

            var strLoad = fs.readFileSync(setLoadFilePath, 'utf-8');
            var divStr: string[] = strLoad.split('\n');
            this._histCodeArray = new Array();

            for (var i = 0; i < divStr.length; i++) {
                if (divStr[i].length > 0) {
                    //console.log(i + " :: " + divStr[i]);

                    var code: string = divStr[i];
                    this._histCodeArray.push(code);
                    if (this._histCodeArray.length > this._histCodeMaxNum) {
                        this._histCodeArray.shift();
                    }
                }
            }
            
        }

        //####
        /*
        console.log("---- LoadStart ---");
        this.UtilDebugPrintCode();
        console.log("---- LoadEnd ---");
        */
    }

    //############ 今現在管理しているコードを一覧表示 ###########
    public UtilDebugPrintCode() {

        console.log("----- " + this._histCodeArray.length + " ------");
        for (var i: number = this._histCodeArray.length - 1; i >= 0; i--)
            console.log(this._histCodeArray[i]);
        console.log("-----------");
    }






}
