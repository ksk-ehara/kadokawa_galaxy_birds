"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UtilAccessCode {
    //####### �R���X�g���N�^ ##############
    constructor() {
        this._idInterval = null;
        this.codeCharNum = 6; //���s�i�����j����R�[�h�̕�����
        this.choiceChar = "abcdefghijklmnopqrstuvwxyz0123456789";
        //#### ���s�����R�[�h�̗���
        this._histCodeMaxNum = 5;
        this._histCodeArray = new Array();
        this._secretAccessCode = "oj5510";
        this.fileBackupEnable = true;
        this._BackupFileName = "./_AccessCode.bk";
        if (this.fileBackupEnable == true)
            this.UtilLoadAccessCode(this._BackupFileName);
    }
    //############# �R�[�h�̔��s���\�b�h #############
    UtilExec(setCallFunc, setInterValSecTime) {
        //### ����
        var code = this._MakeAccessCode();
        setCallFunc(code);
        //### �J��Ԃ�����
        this._idInterval = setInterval(() => {
            var code = this._MakeAccessCode();
            // �R�[���o�b�N�Ăяo��
            setCallFunc(code);
        }, setInterValSecTime);
    }
    _MakeAccessCode() {
        // �������镶����Ɋ܂߂镶���Z�b�g
        var cl = this.choiceChar.length;
        var code = "";
        for (var i = 0; i < this.codeCharNum; i++) {
            code += this.choiceChar[Math.floor(Math.random() * cl)];
        }
        this._histCodeArray.push(code);
        if (this._histCodeArray.length > this._histCodeMaxNum) {
            this._histCodeArray.shift();
        }
        if (this.fileBackupEnable == true)
            this.UtilSaveAccessCode(this._BackupFileName);
        return code;
    }
    //############# �R�[�h�̔��s��~ #############
    UtilStop() {
        if (this._idInterval == null)
            return;
        clearInterval(this._idInterval); //��~����
        this._idInterval = null;
    }
    //############# �Z�b�g���ꂽ�R�[�h���L�������ǂ������`�F�b�N���� ############
    UtilExistsAccessCode(setCheckCode) {
        if (setCheckCode == this._secretAccessCode)
            return true;
        var status = false;
        for (var i = this._histCodeArray.length - 1; i >= 0; i--) {
            if (setCheckCode == this._histCodeArray[i]) {
                status = true;
                break;
            }
        }
        return status;
    }
    //########## �ŐV�̃A�N�Z�X�R�[�h��Ԃ�
    UtilGetNewestAccessCode() {
        if (this._histCodeArray.length == 0)
            return null;
        var code = this._histCodeArray[this._histCodeArray.length - 1];
        return code;
    }
    //�@�t�@�C���ɃA�N�Z�X�R�[�h�������o��
    UtilSaveAccessCode(setSaveFilePath) {
        const fs = require('fs');
        var strSave = "";
        //for (var i: number = this._histCodeArray.length - 1; i >= 0; i--)
        for (var i = 0; i < this._histCodeArray.length; i++)
            strSave += this._histCodeArray[i] + "\n";
        fs.writeFileSync(setSaveFilePath, strSave);
    }
    //�@�A�N�Z�X�R�[�h��ǂݍ���
    UtilLoadAccessCode(setLoadFilePath) {
        const fs = require('fs');
        if (fs.existsSync(setLoadFilePath) == true) {
            var strLoad = fs.readFileSync(setLoadFilePath, 'utf-8');
            var divStr = strLoad.split('\n');
            this._histCodeArray = new Array();
            for (var i = 0; i < divStr.length; i++) {
                if (divStr[i].length > 0) {
                    //console.log(i + " :: " + divStr[i]);
                    var code = divStr[i];
                    this._histCodeArray.push(code);
                    if (this._histCodeArray.length > this._histCodeMaxNum) {
                        this._histCodeArray.shift();
                    }
                }
            }
        }
        //####
        /*
        console.log("---- LoadStart ---");
        this.UtilDebugPrintCode();
        console.log("---- LoadEnd ---");
        */
    }
    //############ �����݊Ǘ����Ă���R�[�h���ꗗ�\�� ###########
    UtilDebugPrintCode() {
        console.log("----- " + this._histCodeArray.length + " ------");
        for (var i = this._histCodeArray.length - 1; i >= 0; i--)
            console.log(this._histCodeArray[i]);
        console.log("-----------");
    }
}
exports.default = UtilAccessCode;
//# sourceMappingURL=UtilAccessCode.js.map