"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const readline = require("readline");
class UtilAppSoftEnv {
    constructor() {
        this.AccessURL = 123;
    }
    Load(setFilePath) {
        var stream = fs.createReadStream(setFilePath);
        var reader = readline.createInterface({ input: stream });
        reader.on("line", (data) => {
            console.log(data);
        });
    }
}
exports.default = UtilAppSoftEnv;
//# sourceMappingURL=UtilAppSoftEnv.js.map