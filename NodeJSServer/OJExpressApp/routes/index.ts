/*
 * GET home page.
 */
import express = require('express');
const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {

    var msg: string = "";
    msg += "<h1>KADOKAWA Contents Site</h1>";

    /*
    msg += "<br/>"
    msg += "http://localhost:10443/pm/index.html?code=oj5510";
    msg += "<br/>"
    msg += "https://pre.clammbon.jp/pm/index.html?code=oj5510";
    msg += "<br/>"
    msg += "https://clammbon.jp/pm/index.html?code=oj5510";
    msg += "<br/>"
    */

    res.send(msg);

});


export default router;