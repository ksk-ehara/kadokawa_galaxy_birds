"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET home page.
 */
const express = require("express");
const router = express.Router();
router.get('/', (req, res) => {
    var msg = "";
    msg += "<h1>KADOKAWA Contents Site</h1>";
    /*
    msg += "<br/>"
    msg += "http://localhost:10443/pm/index.html?code=oj5510";
    msg += "<br/>"
    msg += "https://pre.clammbon.jp/pm/index.html?code=oj5510";
    msg += "<br/>"
    msg += "https://clammbon.jp/pm/index.html?code=oj5510";
    msg += "<br/>"
    */
    res.send(msg);
});
exports.default = router;
//# sourceMappingURL=index.js.map