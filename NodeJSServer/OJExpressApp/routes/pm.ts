﻿/*
 * GET users listing.
 */
import express = require('express');
const router = express.Router();
const fs = require("fs");


router.get('/', (req: express.Request, res: express.Response) => {

    const html = fs.readFileSync("pm/index.html", "utf-8");
    res.send(html);


});

export default router;