"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET users listing.
 */
const express = require("express");
const router = express.Router();
const fs = require("fs");
router.get('/', (req, res) => {
    const html = fs.readFileSync("admin/index.html", "utf-8");
    res.send(html);
});
exports.default = router;
//# sourceMappingURL=admin.js.map